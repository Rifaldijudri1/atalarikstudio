<div class="body">
			<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
				<div class="header-body border-color-primary border-bottom-0 color-menu">
					<div class="header-top header-top-simple-border-bottom color-black">
						<div class="container">
							<div class="header-row py-2">
								<div class="header-column justify-content-start">
									<div class="header-row">
										<nav class="header-nav-top">
											<ul class="nav nav-pills text-uppercase text-2">
												<li class="nav-item nav-item-anim-icon d-none d-md-block">
													<a class="nav-link ps-0" href="<?php echo base_url(); ?>about"><i class="fas fa-angle-right"></i> About Us</a>
												</li>
												<li class="nav-item nav-item-anim-icon d-none d-md-block">
													<a class="nav-link" href="<?php echo base_url(); ?>contact"><i class="fas fa-angle-right"></i> Contact Us</a>
												</li>
											</ul>
										</nav>
									</div>
								</div>
								<div class="header-column justify-content-end">
									<div class="header-row">
										<nav class="header-nav-top">
											<ul class="nav nav-pills">
												<li class="nav-item">
													<a href="#"><i class="far fa-envelope text-4 text-color-primary" style="top: 1px;"></i> <span class="__cf_email__" >
													 <?php foreach ($setting as $a): ?>
															<?php echo $a->email ?>
													<?php endforeach; ?>
												</span></a>
												</li>
												<li class="nav-item">
													<!-- <a href="tel:123-456-7890"><i class="fab fa-whatsapp text-4 text-color-primary" style="top: 0;"></i> 
													 <?php foreach ($setting as $a): ?>
															<?php echo $a->contact_us ?>
													<?php endforeach; ?>

													</a> -->
													<a href="<?php echo base_url(); ?>welcome/sub"><i class="fab fa-elementor text-4 text-color-primary" style="top: 0;"></i>Pre-lauching</a>
												</li>
											</ul>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo">
										<a href="<?php echo base_url(); ?>">
											<img alt="" width="200" height="65" data-sticky-width="82" data-sticky-height="40" src="<?php echo base_url(); ?>/asset1/img/pub/logo_png.png">
										</a>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row">
									<div class="header-nav header-nav-line header-nav-bottom-line">
										<div class="header-nav-main header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
											<nav class="collapse">
												<ul class="nav nav-pills" id="mainNav">
													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle color-font" href="<?php echo base_url(); ?>">
															Home
														</a>
														
													</li>

													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle color-font" href="<?php echo base_url(); ?>about">
															About Us
														</a>
														
													</li>

													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle color-font" href="<?php echo base_url(); ?>caffe">
															Coffe-Work Sky Lounge
														</a>
														
													</li>

													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle color-font" href="#">
															Studio
														</a>
														<ul class="dropdown-menu">
															
															<li>
																<a class="dropdown-item" href="<?php echo base_url(); ?>studio">Creative Studio</a>
															</li>

															<li>
																<a class="dropdown-item" href="<?php echo base_url(); ?>studio/art_studio">Art Studio</a>
															</li>

														

															

														</ul>
													</li>
											

													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle color-font" href="#">
															Telent Program
														</a>
														<ul class="dropdown-menu">
														
															<li><a class="dropdown-item" href="<?php echo base_url(); ?>telent">Modeling</a></li>
															<li><a class="dropdown-item" href="<?php echo base_url(); ?>telent/telent_acting">Acting</a></li>
														</ul>
													</li>

													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle color-font" href="#">
															Gallery
														</a>
														<ul class="dropdown-menu">
														
															<li><a class="dropdown-item" href="<?php echo base_url(); ?>gallery">Foto</a></li>
															<li><a class="dropdown-item" href="<?php echo base_url(); ?>gallery/vidio">Vidio</a></li>
														</ul>
													</li>


													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle color-font" href="<?php echo base_url(); ?>pendaftaran">
															Pendaftaran
														</a>
														
													</li>

													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle color-font" href="<?php echo base_url(); ?>contact">
															Contact Us
														</a>
														
													</li>
												</ul>
											</nav>
										</div>
										
										<button class="btn header-btn-collapse-nav" data-bs-toggle="collapse" data-bs-target=".header-nav-main nav">
											<i class="fas fa-bars"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>