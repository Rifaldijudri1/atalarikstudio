<?php
	$this->load->view('header.php');
	$this->load->view('menu.php');
?>



			<div role="main" class="main">
				<section class="page-header page-header-classic page-header-sm">
					<div class="container">
						<div class="row">
							<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
								 <?php foreach ($caffe as $a): ?>
															
													
								<h1 data-title-border><?php echo $a->nama_coffe ?></h1>

								<?php endforeach; ?>
							</div>
							<div class="col-md-4 order-1 order-md-2 align-self-center">
								<ul class="breadcrumb d-block text-md-end">
									<li><a href="<?php echo base_url(); ?>">Home</a></li>
									<li class="active">Caffe Work Sky Lounge</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

			
		<!-- 		<section class="section section-default border-0 my-5 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="750">
					<div class="container py-4">

						<div class="row align-items-center">
							<div class="col-md-6 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="1000">
								<div class="owl-carousel owl-theme nav-inside mb-0" data-plugin-options="{'items': 1, 'margin': 10, 'animateOut': 'fadeOut', 'autoplay': true, 'autoplayTimeout': 6000, 'loop': true}">
									 <?php foreach ($caffe_img as $a): ?>
										<div>
											<img alt="" class="img-fluid" src="<?php echo base_url(); ?>/images/<?php echo $a->gambar_coffe ?>">
										</div>
									 <?php endforeach; ?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="overflow-hidden mb-2">
									<h2 class="text-color-dark font-weight-normal text-7 mb-0 pt-0 mt-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="1200">
										 <?php foreach ($caffe as $a): ?>
											<strong class="font-weight-extra-bold"><?php echo $a->nama_coffe ?>
											</strong></h2>
										<?php endforeach; ?>
								</div>
								<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1400" style="text-align: justify;">
									
									 <?php foreach ($caffe as $a): ?>
											<?php echo $a->deskripsi_coffe ?>
									  <?php endforeach; ?>

								</p>
							</div>
						</div>

					</div>
				</section> -->

				<div class="container">

					<div class="row py-4">
						<div class="col-lg-8">
							<?php foreach ($caffe as $a): ?>
									<h2 class="font-weight-bold text-8 mt-2 mb-0"><?php echo $a->nama_coffe ?></h2>
							
							<p class="mb-4"><?php echo $a->deskripsi_coffe ?></p>
							<?php endforeach; ?>
							<form class="contact-form" action="#" method="POST">
							
							</form>
						<div class="row">
						
						<?php foreach ($caffe_img as $a): ?>
							<div class="form-group col-lg-5">
								
								<div class="portfolio-item">
										<a href="#">
											<span class="thumb-info thumb-info-lighten border-radius-0">
												<span class="thumb-info-wrapper border-radius-0">
													<img src="<?php echo base_url(); ?>/images/<?php echo $a->gambar_coffe ?>" class="img-fluid border-radius-0" alt="" style="width:100%; height:278px;">

													<span class="thumb-info-title">
														<span class="thumb-info-inner"><?php echo $a->nama_gambar ?></span>
														<!-- <span class="thumb-info-type">Sekolah Akting</span> -->
													</span>
													<span class="thumb-info-action">
														<span class="thumb-info-action-icon bg-white opacity-8"><img alt=""  data-sticky-width="82" data-sticky-height="40" src="<?php echo base_url(); ?>/asset1/img/pub/logo_png.png"></span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>

						 <?php endforeach; ?>
							

							




							</div>

							




						</div>
						<div class="col-lg-4">

							<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="800">
								<h4 class="mt-2 mb-1"><strong>Contact Us</strong></h4>
								<ul class="list list-icons list-icons-style-2 mt-2">
									<?php foreach ($caffe as $a): ?>
										<li><i class="fas fa-map-marker-alt top-6"></i> <strong class="text-dark">Address:</strong> <?php echo $a->alamat_coffe ?></li>
										<li><i class="fas fa-phone top-6"></i> <strong class="text-dark">Phone:</strong> <?php echo $a->contact_coffe ?>	</li>
										<li><i class="fas fa-envelope top-6"></i> <strong class="text-dark">Email:</strong> <a href="#"><span class="__cf_email__" >[<?php echo $a->email_coffe ?>]</span></a></li>
									 <?php endforeach; ?>
								</ul>
							</div>

							<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="950">
								<h4 class="pt-5">Open <strong>Hours</strong></h4>
								<ul class="list list-icons list-dark mt-2">
									<li><i class="far fa-clock top-6"></i> Senin - Jumat 9am to 5pm</li>
									<li><i class="far fa-clock top-6"></i> Sabtu - 9am to 12am</li>
									<li><i class="far fa-clock top-6"></i> Minggu - tutup</li>
								</ul>
							</div>

							

						</div>

					</div>

				</div>

			</div>
				

<?php
	$this->load->view('footer.php');
?>