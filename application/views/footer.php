    <footer id="footer">
                <div class="container">
                    <div class="footer-ribbon">
                        <span>Atalarikworkshop</span>
                    </div>
                    <div class="row py-5 my-4">
                        <div class="col-md-6 mb-4 mb-lg-0">
                            <a href="<?php echo base_url(); ?>" class="logo pe-0 pe-lg-3">
                                <img alt="atalarikworkshop" src="<?php echo base_url(); ?>asset1/img/pub/logo_png.png" class=" bottom-4" height="72">
                            </a>
                            <p class="mt-2 mb-2">sebuah industri untuk mengajarkan para generasi muda menjadi akting,aktor dan modeling</p>
                            <p class="mb-0"><a href="<?php echo base_url(); ?>about" class="btn-flat btn-xs text-color-light"><strong class="text-2">VIEW MORE</strong><i class="fas fa-angle-right p-relative top-1 ps-2"></i></a></p>

                            <br>
                            <ul class="header-social-icons social-icons d-none d-sm-block">
                                            <li class="social-icons-facebook"><a href="https://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                                            <li class="social-icons-twitter"><a href="https://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                                            <li class="social-icons-linkedin"><a href="https://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>


                        </div>
                        <div class="col-md-6">
                            <h5 class="text-3 mb-3">CONTACT US</h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="list list-icons list-icons-lg">
                                        <li class="mb-1"><i class="far fa-dot-circle text-color-primary"></i><p class="m-0">Komplek Cibinong City Center Jl. Raya Tegar Beriman no. 1 blok C21, 16915</p></li>
                                        <li class="mb-1"><i class="fab fa-whatsapp text-color-primary"></i><p class="m-0"><a href="tel:8001234567">(+62) 823-992-99</a></p></li>
                                        <li class="mb-1"><i class="far fa-envelope text-color-primary"></i><p class="m-0"><a href="#"><span class="__cf_email__" >support@atalarikworkshop.com</span></a></p></li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul class="list list-icons list-icons-sm">
                                        <li><i class="fas fa-angle-right"></i><a href="<?php echo base_url(); ?>welcome/faq" class="link-hover-style-1 ms-1"> FAQ's</a></li>
                                        <li><i class="fas fa-angle-right"></i><a href="<?php echo base_url(); ?>contact" class="link-hover-style-1 ms-1"> Sitemap</a></li>
                                        <li><i class="fas fa-angle-right"></i><a href="<?php echo base_url(); ?>contact" class="link-hover-style-1 ms-1"> Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright footer-copyright-style-2">
                    <div class="container py-2">
                        <div class="row py-4">
                            <div class="col d-flex align-items-center justify-content-center">
                                <p>© MRJ Copyright 2021. All Rights Reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        

        <!-- Vendor -->
        <script data-cfasync="false" src="<?php echo base_url(); ?>asset1/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="<?php echo base_url(); ?>asset1/vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>asset1/vendor/jquery.appear/jquery.appear.min.js"></script>
        <script src="<?php echo base_url(); ?>asset1/vendor/jquery.easing/jquery.easing.min.js"></script>
        <script src="<?php echo base_url(); ?>asset1/vendor/jquery.cookie/jquery.cookie.min.js"></script>
        <script src="<?php echo base_url(); ?>asset1/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url(); ?>asset1/vendor/jquery.validation/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>asset1/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
        <script src="<?php echo base_url(); ?>asset1/vendor/jquery.gmap/jquery.gmap.min.js"></script>
        <script src="<?php echo base_url(); ?>asset1/vendor/lazysizes/lazysizes.min.js"></script>
        <script src="<?php echo base_url(); ?>asset1/vendor/isotope/jquery.isotope.min.js"></script>
        <script src="<?php echo base_url(); ?>asset1/vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="<?php echo base_url(); ?>asset1/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url(); ?>asset1/vendor/vide/jquery.vide.min.js"></script>
        <script src="<?php echo base_url(); ?>asset1/vendor/vivus/vivus.min.js"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="<?php echo base_url(); ?>asset1/js/theme.js"></script>

        <!-- Theme Initialization Files -->
        <script src="<?php echo base_url(); ?>asset1/js/theme.init.js"></script>

                <script src="<?php echo base_url(); ?>asset1/js/views/view.contact.js"></script>

        

      
    
</body>


</html>
