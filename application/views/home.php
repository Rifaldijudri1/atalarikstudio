<?php
	$this->load->view('header.php');
	$this->load->view('menu.php');
?>



			<div role="main" class="main">
				<section class="section border-0 m-0 ">
					<div class="img-title">
						<img src="asset1/img/pub/bg_slider1.png" class="img-fluid">

					</div>
					<div class="container h-100">
						<div class="row align-items-center h-100">
							<div class="col m-50">
								<div class="d-flex flex-column align-items-center justify-content-center h-100">
									<h1 class="position-relative text-color-dark text-5 line-height-5 font-weight-semibold px-4 mb-2 appear-animation" data-appear-animation="fadeInDownShorterPlus" data-plugin-options="{'minWindowWidth': 0}">
										<span class="position-absolute right-100pct top-50pct transform3dy-n50">
											<img src="asset1/img/slides/slide-title-border-light.png" class="w-auto appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="250" data-plugin-options="{'minWindowWidth': 0}" alt="" />
										</span>
										 <?php foreach ($slide as $a): ?>
											<?php echo $a->subtitle ?>
										
										<span class="position-absolute left-100pct top-50pct transform3dy-n50">
											<img src="asset1/img/slides/slide-title-border-light.png" class="w-auto appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="250" data-plugin-options="{'minWindowWidth': 0}" alt="" />
										</span>
									</h1>
									<h1 class="text-color-dark font-weight-extra-bold mb-3 appear-animation size-mobile-30" data-appear-animation="blurIn" data-appear-animation-delay="1000" data-plugin-options="{'minWindowWidth': 0}"><?php echo $a->nama_content ?></h1>

									<p class="text-color-dark font-weight-extra-bold mb-0 size-mobile-15" data-plugin-animated-letters data-plugin-options="{'startDelay': 2000, 'minWindowWidth': 0}">"<?php echo $a->deskripsi_slide ?>"</p>
									 <?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
				</section>

				<div class="home-intro light border border-bottom-0 mb-0 m-49">
					<div class="container">

						<div class="row align-items-center">
							<div class="col-lg-8">
								<p class="font-weight-bold text-color-dark">
									 <?php foreach ($syarat as $b): ?>
									<?php echo $b->nama_judul ?>  <span class="highlighted-word highlighted-word-animation-1 text-color-primary font-weight-semibold text-5">&nbsp;Syarat & Ketentuan</span>
									<span><?php echo $b->syarat ?></span>
									 <?php endforeach; ?>
								</p>
							</div>
							<div class="col-lg-4">
								<div class="get-started text-start text-lg-end">
									<a href="<?php echo base_url(); ?>pendaftaran" class="btn btn-primary btn-lg text-3 font-weight-semibold btn-py-2 px-4">Daftar</a>
									
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="container py-5 my-4">
					<div class="row text-center py-3">
						<div class="col-md-10 mx-md-auto">
							<h1 class="word-rotator slide font-weight-bold text-8 mb-3 appear-animation" data-appear-animation="fadeInUpShorter">
								<span>AtalarikWorkshop </span>
								<span class="word-rotator-words bg-primary">
									<b class="is-visible">Akting</b>
									<b>Modeling</b>
									<b>Aktor</b>
									
								</span>
								<span>Untuk Generasi Milineal.</span>
							</h1>
							<p class="lead appear-animation mb-0 " data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
								Alima Production dibentuk tahun 2020, dibawah naungan yayasan Alima Fajar Sentosa berlokasi tetap di kawasan Cibinong City Center, memandang perlu membuat sebuah rumah produksi dimasa bertambah luasnya media entertainment di era digital saat ini dengan adanya medsos dan layanan OTT seperti web tv, web series, dll. 
							</p>
						</div>
					</div>
				</div>

				<section class="section section-height-5 bg-primary border-0 pt-5 m-0 appear-animation" data-appear-animation="fadeIn">
					<div class="container">
						<div class="row mt-4 mt-lg-5">
							<div class="col-lg-4 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200">
								<div class="feature-box">
									<div class="feature-box-icon feature-box-icon-large bg-light mt-1">
										<i class="icons icon-support text-color-primary text-6"></i>
									</div>
									<div class="feature-box-info">
										<h2 class="text-color-light font-weight-bold text-4 line-height-5 mb-1">AKTING</h2>
										<p class="text-color-light opacity-7">kemampuan untuk bereaksi terhadap rangsangan imajiner.  maka seorang aktor harus bisa bereaksi sebaik mungkin pada imaji yang tertangkap oleh inderanya.</p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 appear-animation" data-appear-animation="fadeInUpShorter">
								<div class="feature-box">
									<div class="feature-box-icon feature-box-icon-large bg-light mt-1">
										<i class="icons icon-layers text-color-primary text-6"></i>
									</div>
									<div class="feature-box-info">
										<h2 class="font-weight-bold text-color-light text-4 line-height-5 mb-1">MODELING</h2>
										<p class="text-color-light opacity-7">meniru perilaku dan sikap orang lain, di mana orang yang di modelkan merupakan suatu pola untuk dapat ditiru.</p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
								<div class="feature-box">
									<div class="feature-box-icon feature-box-icon-large bg-light mt-1">
										<i class="icons icon-menu text-color-primary text-5"></i>
									</div>
									<div class="feature-box-info">
										<h2 class="font-weight-bold text-color-light text-4 line-height-5 mb-1">AKTOR</h2>
										<p class="text-color-light opacity-7"> orang yang memainkan peran tertentu dalam suatu aksi panggung atau dapat dikatakan bahwa aktor.</p>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</section>

				<section class="section bg-color-grey-scale-1 border-0 pt-0 pt-md-5 m-0">
					<div class="container">
						<div class="row align-items-center justify-content-center pb-4 pb-lg-0">
							<div class="col-lg-6 order-2 order-lg-1 pe-5 pt-4 pt-lg-0 mt-md-5 mt-lg-0 appear-animation" data-appear-animation="fadeInRightShorter">
								<h2 class="font-weight-normal text-6 mb-3"><strong class="font-weight-extra-bold">Atalarik</strong> workshop</h2>
								<p class="lead">sebuah industri untuk mengajarkan para generasi muda menjadi akting,aktor dan modeling .</p>
								<p class="pb-2 mb-4">Daftarkan segara diri anda untuk belajar bersama Atalarik Syach</p>
								<a href="<?php echo base_url(); ?>pendaftaran" class="btn btn-dark font-weight-semibold btn-py-2 px-5">Daftar Segera</a>
							</div>
							<div class="col-9 col-lg-6 order-1 order-lg-2 scale-6 pb-5 pb-lg-0 mt-0 mt-md-4 mb-5 mb-lg-0">
								<img class="img-fluid appear-animation" src="asset1/img/desktop-device-1.png" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="300" data-plugin-options="{'accY': -400}" alt="">
							</div>
						</div>
					</div>
				</section>

				<div class="container">
					<div class="featured-boxes py-5 mt-5 mb-4">
						<div class="row">
							<div class="col-lg-4 col-sm-6">
								<div class="featured-box featured-box-primary featured-box-effect-1">
									<div class="box-content">
										<i class="icon-featured icons icon-people"></i>
										<h3 class="text-color-primary font-weight-bold text-3 mb-2 mt-3">Akting</h3>
										<p class="px-3">kemampuan untuk bereaksi terhadap rangsangan imajiner.</p>
										<!-- <p><a href="https://www.okler.net/" class="text-dark learn-more font-weight-bold text-2">VIEW MORE <i class="fas fa-chevron-right ms-2"></i></a></p> -->
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6">
								<div class="featured-box featured-box-dark featured-box-effect-1">
									<div class="box-content">
										<i class="icon-featured icons icon-docs"></i>
										<h3 class="font-weight-bold text-3 mb-2 mt-3">Modeling</h3>
										<p class="px-3"> orang yang di modelkan merupakan suatu pola untuk dapat ditiru.</p>
										<!-- <p><a href="https://www.okler.net/" class="text-dark learn-more font-weight-bold text-2">VIEW MORE <i class="fas fa-chevron-right ms-2"></i></a></p> -->
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6">
								<div class="featured-box featured-box-primary featured-box-effect-1">
									<div class="box-content">
										<i class="icon-featured icons icon-trophy"></i>
										<h3 class="text-color-primary font-weight-bold text-3 mb-2 mt-3">Aktor</h3>
										<p class="px-3">Orang yang memainkan peran tertentu dalam suatu aksi panggung.</p>
										<!-- <p><a href="https://www.okler.net/" class="text-dark learn-more font-weight-bold text-2">VIEW MORE <i class="fas fa-chevron-right ms-2"></i></a></p> -->
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>

				<section class="section section-height-3 section-background border-0 m-0 appear-animation" data-appear-animation="fadeIn" style="background-image: url(asset1/img/parallax/parallax-10.jpg); background-size: cover; background-position: center;">
					<div class="container">
						<div class="row">
							<div class="col appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">

								<div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options="{'items': 1, 'loop': false}">
								 <?php foreach ($testi as $a): ?>	
									<div>
										<div class="testimonial testimonial-style-2 testimonial-with-quotes testimonial-quotes-dark mb-0">
											<div class="testimonial-author">
												<img src="asset1/img/avatars/avatar.png" class="img-fluid rounded-circle" alt="">
											</div>
											<blockquote>
												<p class="text-color-dark text-5 line-height-5 mb-0"><?php echo $a->deskripsi ?></p>
											</blockquote>
											<div class="testimonial-author">
												<p><strong class="font-weight-extra-bold text-2">- <?php echo $a->nama ?></strong></p>
											</div>
										</div>
									</div>
									<?php endforeach; ?>
									

								</div>

							</div>
						</div>
					</div>
				</section>

				

<?php
	$this->load->view('footer.php');
?>