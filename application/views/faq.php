<?php
	$this->load->view('header.php');
	$this->load->view('menu.php');
?>



			<div role="main" class="main">
				<section class="page-header page-header-classic page-header-sm">
					<div class="container">
						<div class="row">
							<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
								<h1 data-title-border>FAQ</h1>
							</div>
							<div class="col-md-4 order-1 order-md-2 align-self-center">
								<ul class="breadcrumb d-block text-md-end">
									<li><a href="index">Home</a></li>
									<li class="active">FAQ</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

			
				<section class="section section-default border-0 my-5 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="750" style="padding:0px;">
					<div class="container py-4">

						<div class="row align-items-center">
						
							<div class="col-md-12">
								<div class="overflow-hidden mb-2">
									<h2 class="text-color-dark font-weight-normal text-7 mb-0 pt-0 mt-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="1200">Alima <strong class="font-weight-extra-bold">Production</strong></h2>
								</div>
								<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1400" style="text-align: justify;">
									
									 <?php foreach ($setting as $a): ?>
															<?php echo $a->faq ?>
									  <?php endforeach; ?>

								</p>
							</div>
						</div>

					</div>
				</section>
				

<?php
	$this->load->view('footer.php');
?>