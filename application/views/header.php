<!DOCTYPE html>
<html>
    
<head>
        
        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <title>AtalarikWorkshop</title> 

        <meta name="keywords" content="Atalarikworkshop" />
        <meta name="description" content="Balajar akting dan modeling gratis">
        <meta name="author" content="atalarikworkshop.com">
        

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

        <!-- Web Fonts  -->
        <link id="googleFonts" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7CShadows+Into+Light&amp;display=swap" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset1/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset1/vendor/fontawesome-free/css/all.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset1/vendor/animate/animate.compat.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset1/vendor/simple-line-icons/css/simple-line-icons.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset1/vendor/owl.carousel/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset1/vendor/owl.carousel/assets/owl.theme.default.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset1/vendor/magnific-popup/magnific-popup.min.css">

        <!-- Theme CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset1/css/style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset1/css/theme.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset1/css/theme-elements.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset1/css/theme-blog.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset1/css/theme-shop.css">

        <!-- Skin CSS -->
        <link id="skinCSS" rel="stylesheet" href="<?php echo base_url(); ?>asset1/css/skins/default.css">

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset1/css/custom.css">

        <!-- Head Libs -->
        <script src="<?php echo base_url(); ?>asset1/vendor/modernizr/modernizr.min.js"></script>


        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-SEP1T05Z5V"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-SEP1T05Z5V');
        </script>

    </head>

    <body data-plugin-page-transition>

