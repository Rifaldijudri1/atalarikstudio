<?php
	$this->load->view('header.php');
	$this->load->view('menu.php');
?>



			<div role="main" class="main">
						<section class="page-header page-header-classic page-header-sm">
							<div class="container">
								<div class="row">
									<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
										 
																	
															
										<h1 data-title-border>Gallery Vidio</h1>

										
									</div>
									<div class="col-md-4 order-1 order-md-2 align-self-center">
										<ul class="breadcrumb d-block text-md-end">
											<li><a href="<?php echo base_url(); ?>">Home</a></li>
											<li class="active">Gallery Vidio</li>
										</ul>
									</div>
								</div>
							</div>
						</section>

			
			<div class="container py-2">

					<ul class="nav nav-pills sort-source sort-source-style-3 justify-content-center" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
						<!-- <li class="nav-item active" data-option-value="*"><a class="nav-link text-1 text-uppercase active" href="#">Show All</a></li>
						<li class="nav-item" data-option-value=".websites"><a class="nav-link text-1 text-uppercase" href="#">Websites</a></li>
						<li class="nav-item" data-option-value=".logos"><a class="nav-link text-1 text-uppercase" href="#">Logos</a></li>
						<li class="nav-item" data-option-value=".brands"><a class="nav-link text-1 text-uppercase" href="#">Brands</a></li>
						<li class="nav-item" data-option-value=".medias"><a class="nav-link text-1 text-uppercase" href="#">Medias</a></li> -->
					</ul>

					<div class="sort-destination-loader sort-destination-loader-showing mt-4 pt-2">
						<div class="row portfolio-list sort-destination" data-sort-id="portfolio">
							

						 <?php foreach ($foto as $a): ?>
							<div class="col-sm-6 col-lg-4 isotope-item brands" style="background: #fbfbfb; border-radius: 5px;">
								<div class="portfolio-item">
									<a href="#">
										<span class="">
											<span class="thumb-info-wrapper border-radius-0">
												<video  controls  style="width:100%; height:278px;">
                                                        <source src="<?php echo base_url(); ?>/images/gallery/<?php echo $a->file_gallery ?>" type="video/mp4">
                                                        <source src="<?php echo base_url(); ?>/images/gallery/<?php echo $a->file_gallery ?>" type="video/ogg">
                                                      
                                                      </video>


												<span class="thumb-info-title">
													<span class="thumb-info-inner"><?php echo $a->nama_foto ?></span><br>
													<span class="thumb-info-type"><?php echo $a->deskripsi ?></span>
												</span>
												
											</span>
										</span>
									</a>
								</div>
							</div>
							<?php endforeach; ?>

							
							

							
						</div>
					</div>

				</div>

			</div>
				
<br><br><br>
<?php
	$this->load->view('footer.php');
?>