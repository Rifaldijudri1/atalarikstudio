<?php
    $this->load->view('include-admin/header-admin.php');
?>
      <div class="content" id="tampil">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="card data-tables">
                                    <div style="padding:10px; ">
                                      <!-- <button  class="btn btn-default btn-sm float-right col-md-2" data-toggle="modal" data-target="#exampleModal" style="background:#1b365d; ">Tambah Data </button> -->
                                    </div>
                                <div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
                                    <div class="toolbar">
                                        
                                       
                                    </div>
                                    <?php
                               
                                    if($this->session->flashdata('message')){ // Jika ada
                                        echo '<div class="alert alert-success">'.$this->session->flashdata('message').'</div>'; // Tampilkan pesannya
                                     }
                                
                                    ?>


                                    <div class="fresh-datatables">
                                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                            <thead>
                                                <tr>
                                                   
                                                    <th>Contact US</th>
                                                    <th>Email</th>
                                                     <th>Alamat</th>
                                                
                                                   
                                                    <th class="disabled-sorting text-right">#</th>
                                                </tr>
                                            </thead>
                                            <tbody >

                                              <?php foreach ($setting as $a): ?>
                                                  
                                                  <tr>

                                                    
                                                    <td><?php echo $a->contact_us ?></td>
                                                     <td><?php echo $a->email ?></td>
                                                      <td><?php echo $a->address ?></td>
                                                    
                                                    <td><a href="#" class="btn btn-info btn-xs item_edit" data-toggle="modal" data-target="#exampleModal_<?php echo $a->id_websetting ?>"><i class="fa fa-edit"></i></a></td>

                                                  </tr>


                                                   <div class="modal fade bd-example-modal-lg" id="exampleModal_<?php echo $a->id_websetting ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Edit Data </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                            </button>
                                                          </div>
                                                          <div class="modal-body">
                                                     
                                                            <form action="<?php echo base_url(); ?>admin/home/update_contact_us" method="post">
                                                                <input type="hidden" name="id_websetting" value="<?php echo $a->id_websetting ?>">

                                                                <div class="form-group">
                                                                  <label for="exampleInputPassword1">Contact US</label>
                                                                  <input type="text" class="form-control" name="contact_us" id="contact_us" value="<?php echo $a->contact_us ?>">
                                                                  
                                                                </div>

                                                                <div class="form-group">
                                                                  <label for="exampleInputPassword1">Email</label>
                                                                  <input type="text" class="form-control" name="email" id="email" value="<?php echo $a->email ?>">
                                                                  
                                                                </div>

                                                                <div class="form-group">
                                                                  <label for="exampleInputPassword1">Alamat</label>
                                                                 <textarea name="address" class="form-control" id="address"><?php echo $a->address ?></textarea>
                                                                  
                                                                </div>


                                                                

                                                            </div>
                                                            <div class="modal-footer">
                                                                <input type="hidden" name="id_barang" id="id_barang" />
                                                                  <div id="div_action_tambah" >
                                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                  <button type="submit" class="btn btn-primary" >Edit </button>
                                                               </div>

                                                           </form>
                                                          </div>

                                                        </div>
                                                      </div>
                                         </div>

                                              

                                              <?php endforeach; ?>
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



          


          
   



    

<?php
    $this->load->view('include-admin/footer-admin.php');
?>