<?php
    $this->load->view('include-admin/header-admin.php');
?>
      <div class="content" id="tampil">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="card data-tables">
                                    <div style="padding:10px; ">
                                      <!-- <button  class="btn btn-default btn-sm float-right col-md-2" data-toggle="modal" data-target="#exampleModal" style="background:#1b365d; ">Tambah Data </button> -->
                                    </div>
                                <div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
                                    <div class="toolbar">
                                        
                                       
                                    </div>
                                    <?php
                               
                                    if($this->session->flashdata('message')){ // Jika ada
                                        echo '<div class="alert alert-success">'.$this->session->flashdata('message').'</div>'; // Tampilkan pesannya
                                     }
                                
                                    ?>


                                    <div class="fresh-datatables">
                                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                            <thead>
                                                <tr>
                                                   
                                                    <th>Nama Coffe</th>
                                                    <th>Deskripsi Coffe</th>
                                                    <th>Alamat</th>
                                                    <th>Contact</th>
                                                     <th>email</th>
                                                   
                                                   
                                                    <th class="disabled-sorting text-right">#</th>
                                                </tr>
                                            </thead>
                                            <tbody >

                                              <?php foreach ($caffe as $a): ?>
                                                  
                                                  <tr>

                                                    
                                                    <td><?php echo $a->nama_coffe ?></td>
                                                    <td><?php echo $a->deskripsi_coffe ?></td>
                                                    <td><?php echo $a->alamat_coffe ?></td>
                                                    <td><?php echo $a->contact_coffe ?></td>
                                                    <td><?php echo $a->email_coffe ?></td>
                                                   
                                                    <td>
                                                      <a href="#" class="btn btn-info btn-xs item_edit" data-toggle="modal" data-target="#exampleModal_<?php echo $a->id_coffe ?>"><i class="fa fa-edit"></i></a>



                                                    </td>

                                                  </tr>


                                                   <div class="modal fade bd-example-modal-lg" id="exampleModal_<?php echo $a->id_coffe ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Edit Data </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                            </button>
                                                          </div>
                                                          <div class="modal-body">
                                                     
                                                            <form action="<?php echo base_url(); ?>admin/home/update_coffe" method="post">
                                                                <input type="hidden" name="id_coffe" value="<?php echo $a->id_coffe ?>">

                                                               <div class="form-group">
                                                                  <label for="exampleInputPassword1">Nama Coffe</label>
                                                              
                                                                   <input type="text" name="nama_coffe" class="form-control" id="nama_coffe" value="<?php echo $a->nama_coffe ?>">
                                                                  
                                                                </div>

                                                                <div class="form-group">
                                                                  <label for="exampleInputPassword1">Deskripsi Coffe</label>
                                                              
                                                                   <input type="text" name="deskripsi_coffe" class="form-control" id="deskripsi_coffe" value="<?php echo $a->deskripsi_coffe ?>">
                                                                  
                                                                </div>

                                                                 <div class="form-group">
                                                                  <label for="exampleInputPassword1">Alamat Coffe</label>
                                                              
                                                                   <input type="text" name="alamat_coffe" class="form-control" id="alamat_coffe" value="<?php echo $a->alamat_coffe?>">
                                                                  
                                                                </div>

                                                                <div class="form-group">
                                                                  <label for="exampleInputPassword1">Contact Coffe</label>
                                                              
                                                                   <input type="text" name="contact_coffe" class="form-control" id="contact_coffe" value="<?php echo $a->contact_coffe ?>">
                                                                  
                                                                </div>

                                                                 <div class="form-group">
                                                                  <label for="exampleInputPassword1">Email Coffe</label>
                                                              
                                                                   <input type="text" name="email_coffe" class="form-control" id="email_coffe" value="<?php echo $a->email_coffe ?>">
                                                                  
                                                                </div>




                                                                

                                                            </div>
                                                            <div class="modal-footer">
                                                                <input type="hidden" name="id_barang" id="id_barang" />
                                                                  <div id="div_action_tambah" >
                                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                  <button type="submit" class="btn btn-primary" >Edit </button>
                                                               </div>

                                                           </form>
                                                          </div>

                                                        </div>
                                                      </div>
                                         </div>

                                              

                                              <?php endforeach; ?>
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>



                         <div class="col-md-12">

                            <div class="card data-tables">
                                    <div style="padding:10px; ">
                                      <button  class="btn btn-default btn-sm float-right col-md-2" data-toggle="modal" data-target="#tambahGambar" style="background:#1b365d; ">Tambah Gambar </button> 
                                    </div>
                                <div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
                                    <div class="toolbar">
                                        
                                       
                                    </div>
                                    


                                    <div class="fresh-datatables">
                                        <table id="datatables1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                            <thead>
                                                <tr>
                                                   
                                                    <th>Gambar</th>
                                                   
                                                     <th>Nama </th>
                                                   
                                                   
                                                    <th class="disabled-sorting text-right">#</th>
                                                </tr>
                                            </thead>
                                            <tbody >

                                              <?php foreach ($caffe_img as $a): ?>
                                                  
                                                  <tr>

                                                    
                                                    
                                                    
                                                    <td><img  class="img-rounded" src="<?php echo base_url(); ?>/images/<?php echo $a->gambar_coffe ?>" style="width:150px; height:100px; "></td>
                                                    <td><?php echo $a->nama_gambar ?></td>
                                                   
                                                    <td>
                                                      <a href="#" class="btn btn-info btn-xs item_edit" data-toggle="modal" data-target="#exampleModal_<?php echo $a->id_gambar_coffe ?>"><i class="fa fa-edit"></i></a>

                                                        <a href="<?php echo base_url(); ?>/admin/home/delete_gallery_coffe/<?php echo $a->id_gambar_coffe ?>" class="btn btn-danger btn-xs item_hapus" ><i class="fa fa-trash"></i></a>

                                                    </td>

                                                  </tr>


                                                   <div class="modal fade bd-example-modal-lg" id="exampleModal_<?php echo $a->id_gambar_coffe ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Edit Data Gambar </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                            </button>
                                                          </div>
                                                          <div class="modal-body">
                                                     
                                                            <form action="<?php echo base_url(); ?>admin/home/update_caffe_gambar" method="post" enctype="multipart/form-data">
                                                                <input type="hidden" name="id_gambar_coffe" value="<?php echo $a->id_gambar_coffe ?>">

                                                              
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">Upload Gambar</label>
                                                                    <input type="file" name="gambar_cof" class="form-control" >
                                                                  
                                                                    
                                                                  </div>

                                                               <div class="form-group">
                                                                    <label for="exampleInputPassword1">Nama Gambar</label>
                                                                    <input type="text" name="nama_gambar" class="form-control" id="nama_gambar" value="<?php echo $a->nama_gambar ?>">
                                                                    
                                                                  </div>



                                                                
                                                                  <input type="hidden" name="filelama" class="form-control" id="filelama" value="<?php echo $a->gambar_coffe ?>">

                                                            </div>
                                                            <div class="modal-footer">
                                                                <input type="hidden" name="id_barang" id="id_barang" />
                                                                  <div id="div_action_tambah" >
                                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                  <button type="submit" class="btn btn-primary" >Edit </button>
                                                               </div>

                                                           </form>
                                                          </div>

                                                        </div>
                                                      </div>
                                         </div>

                                              

                                              <?php endforeach; ?>
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="modal fade bd-example-modal-lg" id="tambahGambar" tabindex="-1" role="dialog" aria-labelledby="tambahGambar" aria-hidden="true">
                                  <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Tambah Gambar </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                 
                                        <form action="<?php echo base_url(); ?>admin/home/do_upload" method="post" enctype="multipart/form-data">
                                          
                                            <div class="form-group">
                                              <label for="exampleInputPassword1">Upload Gambar</label>
                                              <input type="file" name="gambar_cof" class="form-control" >
                                              
                                            </div>

                                         <div class="form-group">
                                              <label for="exampleInputPassword1">Nama Gambar</label>
                                              <input type="text" name="nama_gambar" class="form-control" id="nama_gambar">
                                              
                                            </div>


                                            

                                        </div>
                                        <div class="modal-footer">
                                           
                                              <div id="div_action_tambah" >
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                              <button type="submit" class="btn btn-primary" >Simpan </button>
                                           </div>

                                       </form>
                                      </div>

                                    </div>
                                  </div>




                    </div>
                </div>
            </div>


                        






          


          
   



    

<?php
    $this->load->view('include-admin/footer-admin.php');
?>