<?php
    $this->load->view('include-admin/header-admin.php');
?>
      <div class="content" id="tampil">
                <div class="container-fluid">
                    <div class="row">


                         <div class="col-md-12">

                            <div class="card data-tables">
                                    <div style="padding:10px; ">
                                      <button  class="btn btn-default btn-sm float-right col-md-2" data-toggle="modal" data-target="#tambahGambar" style="background:#1b365d; ">Tambah Gambar </button> 
                                    </div>
                                <div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
                                    <div class="toolbar">
                                        
                                       
                                    </div>
                                    


                                    <div class="fresh-datatables">
                                        <table id="datatables1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                            <thead>
                                                <tr>
                                                   
                                                    <th>Vidio</th>
                                                    <th>Nama </th>
                                                    <th>Deskripsi</th>

                                                   
                                                    <th class="disabled-sorting text-right">#</th>
                                                </tr>
                                            </thead>
                                            <tbody >

                                              <?php foreach ($vidio as $a): ?>
                                                  
                                                  <tr>

                                                    
                                                    
                                                    
                                                    <td>

                                                      <video width="320" height="240" controls>
                                                        <source src="<?php echo base_url(); ?>/images/gallery/<?php echo $a->file_gallery ?>" type="video/mp4">
                                                        <source src="<?php echo base_url(); ?>/images/gallery/<?php echo $a->file_gallery ?>" type="video/ogg">
                                                      
                                                      </video>



                                                    </td>
                                                    <td><?php echo $a->nama_foto ?></td>
                                                      <td><?php echo $a->deskripsi ?></td>
                                                   
                                                    <td>
                                                       <input type="hidden" name="filelama" class="form-control" id="filelama" value="<?php echo $a->file_gallery ?>">
                                                      <a href="#" class="btn btn-info btn-xs item_edit" data-toggle="modal" data-target="#exampleModal_<?php echo $a->id_foto ?>"><i class="fa fa-edit"></i></a>

                                                        <a href="<?php echo base_url(); ?>/admin/gallery/delete_vidio1/<?php echo $a->id_foto ?>/<?php echo $a->file_gallery ?>" class="btn btn-danger btn-xs item_hapus" ><i class="fa fa-trash"></i></a>

                                                    </td>

                                                  </tr>


                                                   <div class="modal fade bd-example-modal-lg" id="exampleModal_<?php echo $a->id_foto ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Edit Data Vidio </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                            </button>
                                                          </div>
                                                          <div class="modal-body">
                                                     
                                                            <form action="<?php echo base_url(); ?>admin/gallery/update_telent1" method="post" enctype="multipart/form-data">
                                                                <input type="hidden" name="id_foto" value="<?php echo $a->id_foto ?>">

                                                              
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">Upload Vidio</label>
                                                                    <input type="file" name="gambar_cof" class="form-control" >
                                                                  
                                                                    
                                                                  </div>

                                                               <div class="form-group">
                                                                    <label for="exampleInputPassword1">Nama Vidio</label>
                                                                    <input type="text" name="nama_foto" class="form-control" id="nama_foto" value="<?php echo $a->nama_foto ?>">
                                                                    
                                                                  </div>

                                                                    <div class="form-group">
                                                                    <label for="exampleInputPassword1">Deskripsi Vidio</label>
                                                                    <input type="text" name="deskripsi" class="form-control" id="deskripsi" value="<?php echo $a->deskripsi ?>">
                                                                    
                                                                  </div>

                                                                   <input type="hidden" name="status" class="form-control" id="status" value="vidio">

                                                                
                                                                  <input type="hidden" name="filelama" class="form-control" id="filelama" value="<?php echo $a->file_gallery ?>">

                                                            </div>
                                                            <div class="modal-footer">
                                                                <input type="hidden" name="id_barang" id="id_barang" />
                                                                  <div id="div_action_tambah" >
                                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                  <button type="submit" class="btn btn-primary" >Edit </button>
                                                               </div>

                                                           </form>
                                                          </div>

                                                        </div>
                                                      </div>
                                         </div>

                                              

                                              <?php endforeach; ?>
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="modal fade bd-example-modal-lg" id="tambahGambar" tabindex="-1" role="dialog" aria-labelledby="tambahGambar" aria-hidden="true">
                                  <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Tambah Vidio </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                 
                                        <form action="<?php echo base_url(); ?>admin/gallery/insert_vidio" method="post" enctype="multipart/form-data">
                                          
                                            <div class="form-group">
                                              <label for="exampleInputPassword1">Upload Vidio</label>
                                              <input type="file" name="gambar_cof" class="form-control" >
                                              
                                            </div>

                                         <div class="form-group">
                                              <label for="exampleInputPassword1">Nama Vidio</label>
                                              <input type="text" name="nama_foto" class="form-control" id="nama_foto">
                                              
                                            </div>

                                          <div class="form-group">
                                              <label for="exampleInputPassword1">Deskripsi Vidio</label>
                                              <input type="text" name="deskripsi" class="form-control" id="deskripsi">
                                              
                                            </div>

                                              <input type="hidden" name="status" class="form-control" value="vidio">
                                            

                                        </div>
                                        <div class="modal-footer">
                                           
                                              <div id="div_action_tambah" >
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                              <button type="submit" class="btn btn-primary" >Simpan </button>
                                           </div>

                                       </form>
                                      </div>

                                    </div>
                                  </div>




                    </div>
                </div>
            </div>


                        






          


          
   



    

<?php
    $this->load->view('include-admin/footer-admin.php');
?>