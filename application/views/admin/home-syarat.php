<?php
    $this->load->view('include-admin/header-admin.php');
?>
      <div class="content" id="tampil">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="card data-tables">
                                    <div style="padding:10px; ">
                                      <!-- <button  class="btn btn-default btn-sm float-right col-md-2" data-toggle="modal" data-target="#exampleModal" style="background:#1b365d; ">Tambah Data </button> -->
                                    </div>
                                <div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
                                    <div class="toolbar">
                                        
                                       
                                    </div>
                                    <?php
                               
                                    if($this->session->flashdata('message')){ // Jika ada
                                        echo '<div class="alert alert-success">'.$this->session->flashdata('message').'</div>'; // Tampilkan pesannya
                                     }
                                
                                    ?>


                                    <div class="fresh-datatables">
                                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                            <thead>
                                                <tr>
                                                   
                                                    <th>Nama Judul</th>
                                                    <th>Syarat & Ketentuan</th>
                                                   
                                                    <th class="disabled-sorting text-right">#</th>
                                                </tr>
                                            </thead>
                                            <tbody >

                                              <?php foreach ($syarat as $a): ?>
                                                  
                                                  <tr>

                                                    
                                                    <td><?php echo $a->nama_judul ?></td>
                                                    <td><?php echo $a->syarat ?></td>
                                                    <td><a href="#" class="btn btn-info btn-xs item_edit" data-toggle="modal" data-target="#exampleModal_<?php echo $a->id_syarat ?>"><i class="fa fa-edit"></i></a></td>

                                                  </tr>


                                                   <div class="modal fade bd-example-modal-lg" id="exampleModal_<?php echo $a->id_syarat ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Edit Data </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                            </button>
                                                          </div>
                                                          <div class="modal-body">
                                                     
                                                            <form action="<?php echo base_url(); ?>admin/home/update_syarat" method="post">
                                                                <input type="hidden" name="id_syarat" value="<?php echo $a->id_syarat ?>">

                                                                <div class="form-group">
                                                                  <label for="exampleInputPassword1">Nama Judul</label>
                                                                  <input type="text" class="form-control" id="nama_judul" name="nama_judul" value="<?php echo $a->nama_judul ?>">
                                                                </div>


                                                                <div class="form-group">
                                                                  <label for="exampleInputPassword1">Syarat & Ketentuan</label>
                                                                  <input type="text" class="form-control" id="syarat" name="syarat" value="<?php echo $a->syarat ?>">
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <input type="hidden" name="id_barang" id="id_barang" />
                                                                  <div id="div_action_tambah" >
                                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                  <button type="submit" class="btn btn-primary" >Edit </button>
                                                               </div>

                                                           </form>
                                                          </div>

                                                        </div>
                                                      </div>
                                         </div>

                                              

                                              <?php endforeach; ?>
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



          


          
   



    

<?php
    $this->load->view('include-admin/footer-admin.php');
?>