<?php
    $this->load->view('include-admin/header-admin.php');
?>
      <div class="content" id="tampil">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="card data-tables">
                                    <div style="padding:10px; ">
                                      <!-- <button  class="btn btn-default btn-sm float-right col-md-2" data-toggle="modal" data-target="#exampleModal" style="background:#1b365d; ">Tambah Data </button> -->
                                    </div>
                                <div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
                                    <div class="toolbar">
                                        
                                       
                                    </div>
                                    <?php
                               
                                    if($this->session->flashdata('message')){ // Jika ada
                                        echo '<div class="alert alert-success">'.$this->session->flashdata('message').'</div>'; // Tampilkan pesannya
                                     }
                                
                                    ?>


                                    <div class="fresh-datatables">
                                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                            <thead>
                                                <tr>
                                                   
                                                    <th>Nama </th>
                                                    <th>Email</th>
                                                    <th>No telpn</th>
                                                    <th>Umur</th>
                                                    <th>NIK</th>
                                                    <th>Alamat</th>
                                                    <th>Tanggal</th>
                                                   
                                                   
                                                    <th class="disabled-sorting text-right">#</th>
                                                </tr>
                                            </thead>
                                            <tbody >

                                              <?php foreach ($daftar as $a): ?>
                                                  
                                                  <tr>

                                                    
                                                    <td><?php echo $a->nama_cust ?></td>
                                                    <td><?php echo $a->email_cust ?></td>
                                                    <td><?php echo $a->no_telp ?></td>
                                                    <td><?php echo $a->umur ?></td>
                                                    <td><?php echo $a->nik ?></td>
                                                    <td><?php echo $a->alamat_cust ?></td>
                                                      <td><?php echo $a->created_at ?></td>
                                                  
                                                   
                                                    <td>
                                                      <a href="#" class="btn btn-info btn-xs item_edit" data-toggle="modal" data-target="#exampleModal_<?php echo $a->id_pendaftaran ?>"><i class="fa fa-eye"></i></a>



                                                    </td>

                                                  </tr>


                                                   <div class="modal fade bd-example-modal-lg" id="exampleModal_<?php echo $a->id_pendaftaran ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Detail Data </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                            </button>
                                                          </div>
                                                          <div class="modal-body">
                                                     
                                                            <form action="#" method="post">
                                                                <input type="hidden" name="id_pendaftaran" value="<?php echo $a->id_pendaftaran ?>">

                                                               <div class="form-group">
                                                                  <label for="exampleInputPassword1">Nama </label>
                                                              
                                                                   <input type="text" name="nama_cust" class="form-control" id="nama_cust" value="<?php echo $a->nama_cust ?>" disabled="disabled">
                                                                  
                                                                </div>

                                                                <div class="form-group">
                                                                  <label for="exampleInputPassword1">NIK </label>
                                                              
                                                                   <input type="text" name="nik" class="form-control" id="nik" value="<?php echo $a->nik ?>" disabled="disabled">
                                                                  
                                                                </div>

                                                                 <div class="form-group">
                                                                  <label for="exampleInputPassword1">Email </label>
                                                                   <input type="text" name="email_cust" class="form-control" id="email_cust" value="<?php echo $a->email_cust ?>" disabled="disabled">
                                                                  
                                                                </div>

                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                   <div class="form-group">
                                                                     
                                                                        <label for="exampleInputPassword1">Foto KTP </label>
                                                                        <br>
                                                                         <img  class="img-rounded" src="<?php echo base_url(); ?>/images/gallery/<?php echo $a->foto_ktp ?>" style="width:250px; height:200px; ">
                                                                     </div>
                                                                </div>

                                                                 <div class="col-md-6">
                                                                   <div class="form-group">
                                                                     
                                                                        <label for="exampleInputPassword1">Foto Diri </label>
                                                                        <br>
                                                                         <img  class="img-rounded" src="<?php echo base_url(); ?>/images/gallery/<?php echo $a->foto_cust ?>" style="width:250px; height:200px; ">
                                                                     </div>
                                                                </div>
                                                               </div> 


                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                   <div class="form-group">
                                                                     
                                                                        <label for="exampleInputPassword1">Foto KK </label>
                                                                        <br>
                                                                         <img  class="img-rounded" src="<?php echo base_url(); ?>/images/gallery/<?php echo $a->foto_kk ?>" style="width:250px; height:200px; ">
                                                                     </div>
                                                                </div>

                                                                 <div class="col-md-6">
                                                                   <div class="form-group">
                                                                     
                                                                        <label for="exampleInputPassword1">Foto Surat Sehat </label>
                                                                        <br>
                                                                         <img  class="img-rounded" src="<?php echo base_url(); ?>/images/gallery/<?php echo $a->foto_sehat ?>" style="width:250px; height:200px; ">
                                                                     </div>
                                                                </div>

                                                               <!--   <div class="col-md-6">
                                                                   <div class="form-group">
                                                                     
                                                                        <label for="exampleInputPassword1">Foto Surat Antigen </label>
                                                                        <br>
                                                                         <img  class="img-rounded" src="<?php echo base_url(); ?>/images/gallery/<?php echo $a->foto_antigen ?>" style="width:250px; height:200px; ">
                                                                     </div>
                                                                </div> -->
                                                               </div> 
                                                                




                                                                

                                                            </div>
                                                            <div class="modal-footer">
                                                                <input type="hidden" name="id_barang" id="id_barang" />
                                                                  <div id="div_action_tambah" >
                                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                  
                                                               </div>

                                                           </form>
                                                          </div>

                                                        </div>
                                                      </div>
                                         </div>

                                              

                                              <?php endforeach; ?>
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>



                      




                    </div>
                </div>
            </div>


                        






          


          
   



    

<?php
    $this->load->view('include-admin/footer-admin.php');
?>