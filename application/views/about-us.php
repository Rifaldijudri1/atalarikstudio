<?php
	$this->load->view('header.php');
	$this->load->view('menu.php');
?>



			<div role="main" class="main">
				<section class="page-header page-header-classic page-header-sm">
					<div class="container">
						<div class="row">
							<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
								<h1 data-title-border>About Alima Production</h1>
							</div>
							<div class="col-md-4 order-1 order-md-2 align-self-center">
								<ul class="breadcrumb d-block text-md-end">
									<li><a href="index">Home</a></li>
									<li class="active">About</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

			
				<section class="section section-default border-0 my-5 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="750">
					<div class="container py-4">

						<div class="row align-items-center">
							<div class="col-md-6 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="1000">
								<div class="owl-carousel owl-theme nav-inside mb-0" data-plugin-options="{'items': 1, 'margin': 10, 'animateOut': 'fadeOut', 'autoplay': true, 'autoplayTimeout': 6000, 'loop': true}">
									<div>
										<img alt="" class="img-fluid" src="asset1/img/generic/generic-corporate-3-2-full.jpg">
									</div>
									<div>
										<img alt="" class="img-fluid" src="asset1/img/generic/generic-corporate-3-3-full.jpg">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="overflow-hidden mb-2">
									<h2 class="text-color-dark font-weight-normal text-7 mb-0 pt-0 mt-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="1200">Alima <strong class="font-weight-extra-bold">Production</strong></h2>
								</div>
								<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1400" style="text-align: justify;">
									
									 <?php foreach ($setting as $a): ?>
															<?php echo $a->about_us ?>
									  <?php endforeach; ?>

								</p>
							</div>
						</div>

					</div>
				</section>
				

<?php
	$this->load->view('footer.php');
?>