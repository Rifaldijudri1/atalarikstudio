<html lang="zxx">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Atalarikworkshop Admin Page">
    <meta name="keywords" content="Atalarikworkshop Admin Page">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <title>Admin-AtalarikWorkshop</title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link href="<?php echo base_url(); ?>/static\plugin\bootstrap\css\bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/static\plugin\font-awesome\css\fontawesome-all.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/static\plugin\et-line\style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/static\plugin\themify-icons\themify-icons.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/static\plugin\owl-carousel\css\owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/static\plugin\magnific\magnific-popup.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/static\css\style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/static\css\color\default.css" rel="stylesheet" id="color_theme">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
  </head>

 


  