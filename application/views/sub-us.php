<?php
	$this->load->view('header.php');
	$this->load->view('menu.php');
?>



			<div role="main" class="main">
				<section class="page-header page-header-classic page-header-sm">
					<div class="container">
						<div class="row">
							<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
								<h1 data-title-border>Pre-lauching</h1>
							</div>
							<div class="col-md-4 order-1 order-md-2 align-self-center">
								<ul class="breadcrumb d-block text-md-end">
									<li><a href="index">Home</a></li>
									<li class="active">Pre-laucjong</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

			
				<section class=" section-default border-0  appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="750" style="padding:0px; margin-top:-40px; ">
					<div class="container py-4">

					<?php foreach ($sub as $a): ?>
						<div class="row align-items-center" style="border: 1px solid #dfdfdf; padding: 20px;  margin-top: 8px;border-radius: 3px; ">
							<div class="col-md-4">
								<div class="overflow-hidden mb-2">
									<img alt="" class="img-fluid" src="<?php echo base_url(); ?>/images/gallery/<?php echo $a->file ?>" style="height:200px; ">
							</div>
							
							</div>
							<div class="col-md-8">
								<div class="overflow-hidden mb-2">
									<h2 class="text-color-dark font-weight-normal text-7 mb-0 pt-0 mt-0 appear-animation" 	data-appear-animation="maskUp" data-appear-animation-delay="1200">

										<?php echo $a->nama_sub ?>

									 </h2>
								</div>
								<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1400" style="text-align: justify;">
									
									
										<?php echo $a->deskripsi_lengkap ?>
									 

								</p>


								<a href="<?php echo base_url(); ?>pendaftaran" target="_blank" class="btn btn-dark btn-lg text-3 font-weight-semibold px-4 py-3">Daftar</a>
							</div>
						</div>
						 <?php endforeach; ?>

					</div>
				</section>
				

<?php
	$this->load->view('footer.php');
?>