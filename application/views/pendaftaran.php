<?php
	$this->load->view('header.php');
	$this->load->view('menu.php');
?>



			<div role="main" class="main">
						<section class="page-header page-header-classic page-header-sm">
							<div class="container">
								<div class="row">
									<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
										 
																	
															
										<h1 data-title-border>Pendaftaran</h1>

										
									</div>
									<div class="col-md-4 order-1 order-md-2 align-self-center">
										<ul class="breadcrumb d-block text-md-end">
											<li><a href="<?php echo base_url(); ?>">Home</a></li>
											<li class="active">Pendaftaran</li>
										</ul>
									</div>
								</div>
							</div>
						</section>

			
						<div class="container">

					<div class="row py-4">
						<div class="col-lg-12">

							<h2 class="font-weight-bold text-8 mt-2 mb-0">Daftar Sekolah Akting</h2>
							<p class="mb-4">Syarat & Ketentuan Berlaku</p>

							<form action="<?php echo base_url(); ?>pendaftaran/insert_telent1" method="POST" enctype="multipart/form-data">
							
								 <?php
					                // Cek apakah terdapat session nama message
					                if($this->session->flashdata('message')){ // Jika ada
					                  echo '<div class="alert alert-success">'.$this->session->flashdata('message').'</div>'; // Tampilkan pesannya
					                }
              					?>
								
							

								<div class="row">
									<div class="form-group col-lg-6">
										<label class="form-label mb-1 text-2">Full Name</label>
										<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control text-3 h-auto py-2" name="nama_cust" required>
									</div>
									<div class="form-group col-lg-6">
										<label class="form-label mb-1 text-2">Email Address</label>
										<input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control text-3 h-auto py-2" name="email_cust" required>
									</div>
								</div>

								<div class="row">
									<div class="form-group col-lg-4">
										<label class="form-label mb-1 text-2">Nik</label>
										<input type="number" value="" data-msg-required="Please enter your NIK." maxlength="100" class="form-control text-3 h-auto py-2" name="nik" required>
									</div>

									<div class="form-group col-lg-4">
										<label class="form-label mb-1 text-2">Nomor Telpon</label>
										<input type="number" value="" data-msg-required="Please enter your Telpon." maxlength="100" class="form-control text-3 h-auto py-2" name="no_telp" required>
									</div>
									<div class="form-group col-lg-4">
										<label class="form-label mb-1 text-2">Umur</label>
										<input type="number" value="" data-msg-required="Please enter your Umur." maxlength="100" class="form-control text-3 h-auto py-2" name="umur" required>
									</div>

									
								</div>
								
								<div class="row">
									<div class="form-group col-lg-6">
										<label class="form-label mb-1 text-2">Upload Foto Ktp</label>
										<input type="file"  data-msg-required="Please Upload Foto Ktp."  class="form-control text-3 h-auto py-2" name="userfile[]" multiple="multiple" required>
									</div>
									<div class="form-group col-lg-6">
										<label class="form-label mb-1 text-2">Upload Foto Full Body/ Setengah Body</label>
										<input type="file"  data-msg-required="Please Upload Foto."  class="form-control text-3 h-auto py-2" name="userfile[]" multiple="multiple" required>
									</div>

									<div class="form-group col-lg-6">
										<label class="form-label mb-1 text-2">Upload Foto KK (Kartu Keluarga)</label>
										<input type="file"  data-msg-required="Please Upload Foto KK."  class="form-control text-3 h-auto py-2" name="userfile[]" multiple="multiple" required>
									</div>
									<div class="form-group col-lg-6">
										<label class="form-label mb-1 text-2">Upload Foto Surat Keterangan Sehat</label>
										<input type="file"  data-msg-required="Please Upload Foto."  class="form-control text-3 h-auto py-2" name="userfile[]" multiple="multiple" required>
									</div>

									<!-- <div class="form-group col-lg-12">
										<label class="form-label mb-1 text-2">Upload Foto Surat antigen</label>
										<input type="file"  data-msg-required="Please Upload Foto."  class="form-control text-3 h-auto py-2" name="userfile[]" multiple="multiple" required>
									</div> -->

								</div>


								<div class="row">
									<div class="form-group col">
										<label class="form-label mb-1 text-2">Alamat</label>
										<textarea maxlength="5000" data-msg-required="Please enter your alamat." rows="8" class="form-control text-3 h-auto py-2" name="alamat_cust" required></textarea>
									</div>
								</div>
								<div class="row">
									<div class="form-group col">
										

										 <button type="submit" class="btn btn-primary btn-modern" data-loading-text="Loading...">Daftar</button>

									</div>
								</div>
							</form>

						</div>
					</div>
					</div>
				
<br><br><br>
<?php
	$this->load->view('footer.php');
?>