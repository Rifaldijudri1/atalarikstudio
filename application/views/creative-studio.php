<?php
	$this->load->view('header.php');
	$this->load->view('menu.php');
?>



<div role="main" class="main">

				<section class="page-header page-header-modern bg-color-light-scale-1 page-header-md ">
					<div class="container-fluid">
						<div class="row align-items-center">

							<div class="col">
								<div class="row">
									<div class="col-md-12 align-self-center p-static order-2 text-center">
										<div class="overflow-hidden pb-2">
											<h1 class="text-dark font-weight-bold text-9 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="100">Creative Studio</h2>
										</div>
									</div>
									<div class="col-md-12 align-self-center order-1">
										<ul class="breadcrumb d-block text-center appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="300">
											<li><a href="#">Home</a></li>
											<li><a href="#">Studio</a></li>
											<li><a href="#">Creative Studio</a></li>
										
										</ul>
									</div>
								</div>
							</div>

						</div>
					</div>
				</section>

				<div class="container py-2">

					<ul class="nav nav-pills sort-source sort-source-style-3 justify-content-center" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
						<!-- <li class="nav-item active" data-option-value="*"><a class="nav-link text-1 text-uppercase active" href="#">Show All</a></li>
						<li class="nav-item" data-option-value=".websites"><a class="nav-link text-1 text-uppercase" href="#">Websites</a></li>
						<li class="nav-item" data-option-value=".logos"><a class="nav-link text-1 text-uppercase" href="#">Logos</a></li>
						<li class="nav-item" data-option-value=".brands"><a class="nav-link text-1 text-uppercase" href="#">Brands</a></li>
						<li class="nav-item" data-option-value=".medias"><a class="nav-link text-1 text-uppercase" href="#">Medias</a></li> -->
					</ul>

					<div class="sort-destination-loader sort-destination-loader-showing mt-4 pt-2">
						<div class="row portfolio-list sort-destination" data-sort-id="portfolio">
							

						 <?php foreach ($studio as $a): ?>
							<div class="col-sm-6 col-lg-4 isotope-item brands">
								<div class="portfolio-item">
									<a href="#">
										<span class="thumb-info thumb-info-lighten border-radius-0">
											<span class="thumb-info-wrapper border-radius-0">
												<img src="<?php echo base_url(); ?>/images/studio/<?php echo $a->gambar_studio ?>" class="img-fluid border-radius-0" alt="" style="width:100%; height:278px;">

												<span class="thumb-info-title">
													<span class="thumb-info-inner"><?php echo $a->nama_studio ?></span>
													<span class="thumb-info-type"><?php echo $a->deskripsi_studio ?></span>
												</span>
												<span class="thumb-info-action">
													<span class="thumb-info-action-icon bg-white opacity-8"><img alt=""  data-sticky-width="82" data-sticky-height="40" src="<?php echo base_url(); ?>/asset1/img/pub/logo_png.png"></span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>
							<?php endforeach; ?>

							
							

							
						</div>
					</div>

				</div>

			</div>

<?php
	$this->load->view('footer.php');
?>