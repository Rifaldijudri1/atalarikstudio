<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="/static/img/TVRI-logo2.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>AtalarikWorkshop</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>/assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?php echo base_url(); ?>/assets/css/demo.css" rel="stylesheet" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
</head>

<body>
       


    <div class="wrapper">
        <div class="sidebar" data-color="blue" data-image="<?php echo base_url(); ?>/static/img/home-banner-7.png">
           
            <div class="sidebar-wrapper">
                <div class="logo" style="font-size: 9pt;padding: 20px; font-weight:400;">
									<b>ATALARIK WORKSHOP</b>
                </div>
                <div class="user" style="padding-bottom:0px;  margin-top:0px;">
                   
                   
                   
                </div>
                <ul class="nav">

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#componentsExamples">
                            <i class="nc-icon nc-app"></i>
                            <p>
                                Home
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse " id="componentsExamples">
                            <ul class="nav">
                                <li class="nav-item ">
                                    <a class="nav-link" href="<?php echo base_url(); ?>admin/home">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">Slide Content</span>
                                    </a>
                                </li>
                                 <li class="nav-item ">
                                    <a class="nav-link" href="<?php echo base_url(); ?>admin/home/about_us">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">About Us</span>
                                    </a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link" href="<?php echo base_url(); ?>admin/home/caffe">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">Coffe-Work</span>
                                    </a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link" href="<?php echo base_url(); ?>admin/home/contact_us">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">Contact Us</span>
                                    </a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link" href="<?php echo base_url(); ?>admin/home/syarat">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">Syarat & Ketentuan</span>
                                    </a>
                                </li>

                                 <li class="nav-item ">
                                    <a class="nav-link" href="<?php echo base_url(); ?>admin/home/FAQ">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">FAQ</span>
                                    </a>
                                </li>

                                 <li class="nav-item ">
                                    <a class="nav-link" href="<?php echo base_url(); ?>admin/home/sub_us">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">Sub-Us</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                  
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#StudioExamples">
                            <i class="nc-icon nc-app"></i>
                            <p>
                                Studio
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse " id="StudioExamples">
                            <ul class="nav">
                                <li class="nav-item ">
                                    <a class="nav-link" href="<?php echo base_url(); ?>admin/studio/" >
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">Creative Studio</span>
                                    </a>
                                </li>
                                 <li class="nav-item ">
                                    <a class="nav-link" href="<?php echo base_url(); ?>admin/studio/index_art">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">Art Studio</span>
                                    </a>
                                </li>

                              
                            </ul>
                        </div>
                    </li>

                    

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#TelentExamples">
                            <i class="nc-icon nc-app"></i>
                            <p>
                                Telent Program
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse " id="TelentExamples">
                            <ul class="nav">
                                <li class="nav-item ">
                                    <a class="nav-link" href="<?php echo base_url(); ?>admin/telent">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">Modeling</span>
                                    </a>
                                </li>
                                 <li class="nav-item ">
                                    <a class="nav-link" href="<?php echo base_url(); ?>admin/telent/acting">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">Acting</span>
                                    </a>
                                </li>

                              
                            </ul>
                        </div>
                    </li>

                     <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#GalleryExamples">
                            <i class="nc-icon nc-app"></i>
                            <p>
                                Gallery
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse " id="GalleryExamples">
                            <ul class="nav">
                                <li class="nav-item ">
                                    <a class="nav-link" href="<?php echo base_url(); ?>admin/gallery">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">Foto</span>
                                    </a>
                                </li>
                                 <li class="nav-item ">
                                    <a class="nav-link" href="<?php echo base_url(); ?>admin/gallery/vidio">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">Vidio</span>
                                    </a>
                                </li>

                              
                            </ul>
                        </div>
                    </li>

                     <li class="nav-item "  id="li_data_jenis_asset" >
                        <a class="nav-link" href="<?php echo base_url(); ?>admin/pendaftaraan">
                            <i class="nc-icon nc-app"></i>
                            <p>Pendaftaran</p>
                        </a>
                    </li>


                     <li class="nav-item "  id="li_data_jenis_asset" >
                        <a class="nav-link" href="<?php echo base_url(); ?>admin/testimoni">
                            <i class="nc-icon nc-app"></i>
                            <p>Testimoni</p>
                        </a>
                    </li>
                     


                    


                    
                </ul>
            </div>
        </div>

        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg ">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-minimize">
                            <button id="minimizeSidebar" class="btn btn-default btn-fill btn-round btn-icon d-none d-lg-block" style="background:#1b365d; ">
                                <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                                <i class="fa fa-navicon visible-on-sidebar-mini"></i>
                            </button>
                        </div>
                        <a class="navbar-brand" href="#pablo"><?= $judul; ?> </a>
                    </div>
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end">
                        
                        <ul class="navbar-nav">
<!--                         
                            <li class="dropdown nav-item">
                                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                    <i class="nc-icon nc-bell-55"></i>
                                    <span class="notification">5</span>
                                    <span class="d-lg-none">Notification</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Notification 1</a>
                                    <a class="dropdown-item" href="#">Notification 2</a>
                                    <a class="dropdown-item" href="#">Notification 3</a>
                                    <a class="dropdown-item" href="#">Notification 4</a>
                                    <a class="dropdown-item" href="#">Notification 5</a>
                                </ul>
                            </li>
-->
                            <li class="nav-item dropdown">
                                <img src="<?php echo base_url(); ?>assets/img/faces/face-1.jpg"  style="height:40px;" />
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                   <label >Selamat datang <?php echo $this->session->userdata("akses"); ?> ,<b> <?php echo $this->session->userdata("nama"); ?></b></label>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    
                                   
                                    <!-- <a class="dropdown-item" href="#"  onclick="formChangePassword()" >
                                        <i class="nc-icon nc-lock-circle-open"></i>Ubah Password
                                    </a> -->
                                   
                                     <div class="divider"></div>
                                    <a href="<?php echo base_url(); ?>admin/admin_wp/logout"><span  class="dropdown-item text-danger">
                                        <i class="nc-icon nc-button-power"></i> Log out
                                    </span></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->




    <div class="content">
        <div class="container-fluid">
                   

                              
