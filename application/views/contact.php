<?php
	$this->load->view('header.php');
	$this->load->view('menu.php');
?>



			<div role="main" class="main">

				<!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
				<!-- <div id="map" class="google-map mt-0" style="height: 500px;"></div> -->

				<div class="mapouter"><div class="google-map mt-0"><iframe width="2009" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Komplek%20Cibinong%20City%20Center%20Jl.%20Raya%20Tegar%20Beriman%20no.%201%20blok%20C21,%2016915&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="#"></a><br><style>.mapouter{position:relative;text-align:right;height:500px;width:1009px;}</style><a href="https://www.embedgooglemap.net"></a><style>.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:1009px;}</style></div></div>

				<div class="container">

					<div class="row py-4">
						<div class="col-lg-6">

							<h2 class="font-weight-bold text-8 mt-2 mb-0">Testimoni</h2>
							<p class="mb-4">Kirimkan Masukan dan saran mu disini</p>

							<form  action="<?php echo base_url(); ?>contact/insert_testimoni" method="POST">
								
								 <?php
					                // Cek apakah terdapat session nama message
					                if($this->session->flashdata('message')){ // Jika ada
					                  echo '<div class="alert alert-success">'.$this->session->flashdata('message').'</div>'; // Tampilkan pesannya
					                }
              					?>

								<div class="row">
									<div class="form-group col-lg-6">
										<label class="form-label mb-1 text-2">Full Name</label>
										<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control text-3 h-auto py-2" name="nama" required>
									</div>
									<div class="form-group col-lg-6">
										<label class="form-label mb-1 text-2">Email Address</label>
										<input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control text-3 h-auto py-2" name="email" required>
									</div>
								</div>
								<!-- <div class="row">
									<div class="form-group col">
										<label class="form-label mb-1 text-2">Subject</label>
										<input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control text-3 h-auto py-2" name="subject" required>
									</div>
								</div> -->
								<div class="row">
									<div class="form-group col">
										<label class="form-label mb-1 text-2">Message</label>
										<textarea maxlength="5000" data-msg-required="Please enter your deskripsi." rows="8" class="form-control text-3 h-auto py-2" name="deskripsi" required></textarea>
									</div>
								</div>
								<div class="row">
									<div class="form-group col">
										<input type="submit" value="Send Message" class="btn btn-primary btn-modern" data-loading-text="Loading...">
									</div>
								</div>
							</form>

						</div>
						<div class="col-lg-6">

							<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="800">
								<h4 class="mt-2 mb-1">Our <strong>Office</strong></h4>
								<ul class="list list-icons list-icons-style-2 mt-2">
									<li><i class="fas fa-map-marker-alt top-6"></i> <strong class="text-dark">Address:</strong> 
										<?php foreach ($setting as $a): ?>
												[<?php echo $a->address ?>]
									 	<?php endforeach; ?>
									</li>
									<li><i class="fas fa-phone top-6"></i> <strong class="text-dark">Phone:</strong>
										<?php foreach ($setting as $a): ?>
												[<?php echo $a->contact_us ?>]
									 <?php endforeach; ?>
									</li>
									<li><i class="fas fa-envelope top-6"></i> <strong class="text-dark">Email:</strong> <a href="#"><span class="__cf_email__" >
									 <?php foreach ($setting as $a): ?>
												[<?php echo $a->email ?>]
									 <?php endforeach; ?>
									</span></a></li>
								</ul>
							</div>

							<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="950">
								<h4 class="pt-5">Business <strong>Hours</strong></h4>
								<ul class="list list-icons list-dark mt-2">
									<li><i class="far fa-clock top-6"></i> Senin - Jumat 9am to 5pm</li>
									<li><i class="far fa-clock top-6"></i> Sabtu - 9am to 12am</li>
									<li><i class="far fa-clock top-6"></i> Minggu - tutup</li>
								</ul>
							</div>

							

						</div>

					</div>

				</div>

			</div>

			<section class="call-to-action call-to-action-default with-button-arrow content-align-center call-to-action-in-footer">
				<div class="container">
					<div class="row">
						<div class="col-md-9 col-lg-9">
							<div class="call-to-action-content">
								<h2 class="font-weight-normal text-6 mb-0">AtalarikWorkshop <strong class="font-weight-extra-bold">Yuk</strong> Belajar akting dan modeling gratis</h2>
								<p class="mb-0">Syarat & ketentuan berlaku</p>
							</div>
						</div>
						<div class="col-md-3 col-lg-3">
							<div class="call-to-action-btn">
								<a href="<?php echo base_url(); ?>pendaftaran" target="_blank" class="btn btn-dark btn-lg text-3 font-weight-semibold px-4 py-3">Daftar</a><span class="arrow hlb d-none d-md-block" data-appear-animation="rotateInUpLeft" style="top: -40px; left: 70%;"></span>
							</div>
						</div>
					</div>
				</div>
			</section>
				

<?php
	$this->load->view('footer.php');
?>