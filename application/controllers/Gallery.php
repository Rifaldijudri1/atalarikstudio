<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	function __construct(){
		parent::__construct();		
		 $this->load->model('m_gallery');
		  $this->load->model('m_home');
		 $this->load->library('upload');
		 $this->load->helper('string');
 
	}
	
	public function index()
	{
		$data['setting'] = $this->m_home->about_us();
		$data['foto'] = $this->m_gallery->all();
		$this->load->view('g-foto',$data);
	}


	public function vidio()
	{
		$data['setting'] = $this->m_home->about_us();
		$data['foto'] = $this->m_gallery->all_art();
		$this->load->view('g-vidio',$data);
	}


}
