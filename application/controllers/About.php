<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_home');
 
	}
	
	public function index()
	{
		$data['setting'] = $this->m_home->about_us();
		$this->load->view('about-us',$data);
	}


}
