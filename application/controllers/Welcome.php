<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_home');
		
 
	}
	
	public function index()
	{
		$data['slide'] = $this->m_home->slide();
		$data['syarat'] = $this->m_home->syarat();
		$data['setting'] = $this->m_home->about_us();
		$data['testi'] = $this->m_home->testimoni_show();
		$this->load->view('home',$data);
	}

	public function faq()
	{
		$data['slide'] = $this->m_home->slide();
		$data['syarat'] = $this->m_home->syarat();
		$data['setting'] = $this->m_home->about_us();
		$data['testi'] = $this->m_home->testimoni_show();
		$this->load->view('faq',$data);
	}


	public function sub()
		{
			$data['sub'] = $this->m_home->all_sub();
			$data['syarat'] = $this->m_home->syarat();
			$data['setting'] = $this->m_home->about_us();
			$data['testi'] = $this->m_home->testimoni_show();
			$this->load->view('sub-us',$data);
		}



}
