<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_home');
 
	}
	
	public function index()
	{
		$data['setting'] = $this->m_home->about_us();
		$this->load->view('contact',$data);
	}


	public function insert_testimoni()
        {
		      $nama   = $this->input->post('nama');
		      $email   = $this->input->post('email');
		      $deskripsi   = $this->input->post('deskripsi');	
		      $status   = "nonaktif";
		      $created_at   = date('Y-m-d');	


			$data = array(
				'nama'       => $nama,
				'email'       => $email,
				'deskripsi'       => $deskripsi,
				'status'       => $status,
				'created_at'       => $created_at,

			);
			
			$this->session->set_flashdata('message', 'Testimoni anda berhasil di kirim Terimakasih :D');

			$this->m_home->testimoni_all($data);
			redirect('contact');
			      

		  }




}
