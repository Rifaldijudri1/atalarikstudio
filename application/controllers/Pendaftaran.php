<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran extends CI_Controller {

	function __construct(){
		parent::__construct();		
		 $this->load->model('m_daftar');
		  $this->load->model('m_home');
		 $this->load->library('upload');
		 $this->load->helper('string');
 
	}
	
	public function index()
	{
		$data['setting'] = $this->m_home->about_us();
		
		$this->load->view('pendaftaran',$data);
	}

	private function set_upload_options()
	{   
	    //upload an image options
	    $config = array();
	    $config['upload_path'] = './images/gallery';
	    $config['allowed_types'] = 'gif|jpg|png|jpeg';
	    $config['max_size']      = '0';
	    $config['overwrite']     = FALSE;

	    return $config;
	}


		public function insert_telent1()
        {

			$this->load->library('upload');
			$dataInfo = array();
			$files = $_FILES;
			$cpt = count($_FILES['userfile']['name']);
		    for($i=0; $i<$cpt; $i++)
		    {           
		        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
		        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
		        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
		        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
		        $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

		        $this->upload->initialize($this->set_upload_options());
		        $this->upload->do_upload('userfile');
		        $dataInfo[] = $this->upload->data();
		    }

		    $data = array(
		        'nama_cust' => $this->input->post('nama_cust'),
		        'email_cust' => $this->input->post('email_cust'),
		        'nik' => $this->input->post('nik'),
		        'no_telp' => $this->input->post('no_telp'),
		        'umur' => $this->input->post('umur'),
		        'alamat_cust' => $this->input->post('alamat_cust'),
		        'foto_ktp' => $dataInfo[0]['file_name'],
		        'foto_cust' => $dataInfo[1]['file_name'],
		        'foto_kk' => $dataInfo[2]['file_name'],
		        'foto_sehat' => $dataInfo[3]['file_name'],
		        // 'foto_antigen' => $dataInfo[4]['file_name'],
		        'created_at' =>date('Y-m-d')
		     );

		     $this->session->set_flashdata('message', 'Anda Berhasil Melakukan Pendaftaran, Untuk Proses selanjutnya admin akan menghubungi anda terimakasih');

		     $result_set = $this->m_daftar->insert_gambar_telent($data);

        	 redirect('pendaftaran');

		      
		  }




}
