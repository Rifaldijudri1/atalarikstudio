<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_home');
		 $this->load->library('upload');
 
	}
	
	 function index()
	{
		$data['judul'] = 'Home Slide';
		$data['slide'] = $this->m_home->slide();
		$this->load->view('admin/home-admin',$data);
	}



	function update(){

	$id_slide = $this->input->post('id_slide');
	$subtitle = $this->input->post('subtitle');
	$nama_content = $this->input->post('nama_content');
	$deskripsi_slide = $this->input->post('deskripsi_slide');
	 
		$data = array(
			'subtitle' => $subtitle,
			'nama_content' => $nama_content,
			'deskripsi_slide' => $deskripsi_slide
		);
	 
		$where = array(
			'id_slide' => $id_slide
		);
	 
		$this->m_home->update_data($where,$data,'tb_slide');
		$this->session->set_flashdata('message', 'Anda Berhasil Update Data');
		redirect('admin/home');
	}



	 function syarat(){

		$data['judul'] = 'Home Syarat & Ketentuan';
		$data['syarat'] = $this->m_home->syarat();
		$this->load->view('admin/home-syarat',$data);
	}

	function update_syarat(){

		$id_syarat = $this->input->post('id_syarat');
		$nama_judul = $this->input->post('nama_judul');
		$syarat = $this->input->post('syarat');
		
		 
			$data = array(
				'nama_judul' => $nama_judul,
				'syarat' => $syarat
				
			);
		 
			$where = array(
				'id_syarat' => $id_syarat
			);
		 
			$this->m_home->update_data_syarat($where,$data,'tb_syarat');
			$this->session->set_flashdata('message', 'Anda Berhasil Update Data');
			redirect('admin/home/syarat');
		}

	

	    function about_us(){

			$data['judul'] = 'Home About Us';
			$data['setting'] = $this->m_home->about_us();
			$this->load->view('admin/home-about',$data);
		}

		function update_about_us(){

			//alert("TEST ID".$id_websetting);

			$id_websetting = $this->input->post('id_websetting');
			$about_us = $this->input->post('about_us');
			
			
			
			 
				$data = array(
					'about_us' => $about_us,
					
				
				);
			 
				$where = array(
					'id_websetting' => $id_websetting
				);
			 
				$this->m_home->update_data_about_us($where,$data,'tb_websetting');
				$this->session->set_flashdata('message', 'Anda Berhasil Update Data About Us');
				redirect('admin/home/about_us');
			}



			function contact_us(){

				$data['judul'] = 'Home Contact Us';
				$data['setting'] = $this->m_home->contact_us();
				$this->load->view('admin/home-contact',$data);
			}


			function update_contact_us(){

				//alert("TEST ID".$id_websetting);

				$id_websetting = $this->input->post('id_websetting');
				$contact_us = $this->input->post('contact_us');
				$email = $this->input->post('email');
				$address = $this->input->post('address');
				
				
				
				 
					$data = array(
						'contact_us' => $contact_us,
						'email' => $email,
						'address' => $address,
						
					
					);
				 
					$where = array(
						'id_websetting' => $id_websetting
					);
				 
					$this->m_home->update_data_contact_us($where,$data,'tb_websetting');
					$this->session->set_flashdata('message', 'Anda Berhasil Update Data Contact Us');
					redirect('admin/home/contact_us');
				}

			function FAQ(){

				$data['judul'] = 'Home FAQ';
				$data['setting'] = $this->m_home->contact_us();
				$this->load->view('admin/home-faq',$data);
			}


			function update_faq(){

				//alert("TEST ID".$id_websetting);

				$id_websetting = $this->input->post('id_websetting');
				$faq = $this->input->post('faq');
				
				
				
				 
					$data = array(
						'faq' => $faq,
						
						
					
					);
				 
					$where = array(
						'id_websetting' => $id_websetting
					);
				 
					$this->m_home->update_data_contact_us($where,$data,'tb_websetting');
					$this->session->set_flashdata('message', 'Anda Berhasil Update Data FAQ');
					redirect('admin/home/FAQ');
				}


			function caffe(){

				$data['judul'] = 'Home Caffe Work Sky Lounge';
				$data['caffe'] = $this->m_home->caffe();
				$data['caffe_img'] = $this->m_home->caffe_gambar();
				$this->load->view('admin/home-caffe',$data);
			}


			function update_coffe(){

			//alert("TEST ID".$id_websetting);

			$id_coffe = $this->input->post('id_coffe');
			$nama_coffe = $this->input->post('nama_coffe');
			$deskripsi_coffe = $this->input->post('deskripsi_coffe');
			$alamat_coffe = $this->input->post('alamat_coffe');
			$contact_coffe = $this->input->post('contact_coffe');
			$email_coffe = $this->input->post('email_coffe');
			
			
			
			 
				$data = array(
					'nama_coffe' => $nama_coffe,
					'deskripsi_coffe' => $deskripsi_coffe,
					'alamat_coffe' => $alamat_coffe,
					'contact_coffe' => $contact_coffe,
					'email_coffe' => $email_coffe,
					
					
				
				);
			 
				$where = array(
					'id_coffe' => $id_coffe
				);
			 
				$this->m_home->update_data_caffe($where,$data,'tb_coffe');
				$this->session->set_flashdata('message', 'Anda Berhasil Update Caffe');
				redirect('admin/home/caffe');
			}






		


		public function do_upload()
        {
		      $nama_gambar   = $this->input->post('nama_gambar');
		  

		      // get foto
		      $config['upload_path'] = './images';
		      $config['allowed_types'] = 'jpg|png|jpeg|gif';
		      $config['max_size'] = '2048';  //2MB max
		      $config['max_width'] = '4480'; // pixel
		      $config['max_height'] = '4480'; // pixel
		      $config['file_name'] = $_FILES['gambar_cof']['name'];

		      $this->upload->initialize($config);

			    if (!empty($_FILES['gambar_cof']['name'])) {
			        if ( $this->upload->do_upload('gambar_cof') ) {
			            $gambar_coffe = $this->upload->data();
			            $data = array(
			                          'nama_gambar'       => $nama_gambar,
		                           	  'gambar_coffe'       => $gambar_coffe['file_name'],
			                         
			                        );
									$this->m_home->insert_gambar_caffe($data);
		              redirect('admin/home/caffe');
			        }else {
		              die("gagal upload");
			        }
			    }else {
			      echo "tidak masuk";
			    }

		  }


		  public function update_caffe_gambar()
		  {
		      $id_gambar_coffe   = $this->input->post('id_gambar_coffe');
		      $nama_gambar = $this->input->post('nama_gambar');
		    

		      $path = './images/';

		      $kondisi = array('id_gambar_coffe' => $id_gambar_coffe );

		      // get foto
		      $config['upload_path'] = './images';
		      $config['allowed_types'] = 'jpg|png|jpeg|gif';
		      $config['max_size'] = '2048';  //2MB max
		      $config['max_width'] = '4480'; // pixel
		      $config['max_height'] = '4480'; // pixel
		      $config['file_name'] = $_FILES['gambar_cof']['name'];

		      $this->upload->initialize($config);

			    if (!empty($_FILES['gambar_cof']['name'])) {
			        if ( $this->upload->do_upload('gambar_cof') ) {
			            $gambar_coffe = $this->upload->data();
			            $data = array(
			                          'nama_gambar'       => $nama_gambar,
		                              'gambar_coffe'       => $gambar_coffe['file_name'],
			                        
			                        );
		              // hapus foto pada direktori
		              @unlink($path.$this->input->post('filelama'));

									$this->m_home->update_coffe_gambar($data,$kondisi);
		              redirect('admin/home/caffe');
			        }else {
		              die("gagal update");
			        }
			    }else {
			      echo "tidak masuk";
			    }

		  }

		  function delete_gallery_coffe($id_gambar_coffe)
		  {
		      $path = './images/'.$gambar_coffe;
              unlink($path);

		      $where = array('id_gambar_coffe' => $id_gambar_coffe );
		      $this->m_home->delete_coffe($where);
		      return redirect('admin/home/caffe');
		  }







		   function sub_us()
			{
				$data['judul'] = 'SUB MENU';
				$data['sub'] = $this->m_home->all_sub();
				$this->load->view('admin/sub-us',$data);
			}


			public function insert_sub()
		        {
				      $nama_sub   = $this->input->post('nama_sub');
				      $deskripsi_singkat   = $this->input->post('deskripsi_singkat');
				      $deskripsi_lengkap   = $this->input->post('deskripsi_lengkap');
				     
				  

				      // get foto
				      $config['upload_path'] = './images/gallery';
				      $config['allowed_types'] = 'jpg|png|jpeg|gif';
				      $config['max_size'] = '2048';  //2MB max
				      $config['max_width'] = '4480'; // pixel
				      $config['max_height'] = '4480'; // pixel
				      $config['file_name'] = $_FILES['gambar_cof']['name'];

				      $this->upload->initialize($config);

					    if (!empty($_FILES['gambar_cof']['name'])) {
					        if ( $this->upload->do_upload('gambar_cof') ) {
					            $file = $this->upload->data();
					            $data = array(
					                          'nama_sub'       			=> $nama_sub,
					                          'deskripsi_singkat'       => $deskripsi_singkat,
					                          'deskripsi_lengkap'       => $deskripsi_lengkap,
				                           	  'file'      		 => $file['file_name'],
				                           	  'created_at' =>date('Y-m-d')
					                         
					                        );
											$this->m_home->insert_gambar_sub($data);
				              redirect('admin/home/sub_us');
					        }else {
				              die("gagal upload");
					        }
					    }else {
					      echo "tidak masuk";
					    }

				  }


				  public function update_sub()
					  {
					      $id_sub   = $this->input->post('id_sub');
					      $nama_sub   = $this->input->post('nama_sub');
					      $deskripsi_singkat   = $this->input->post('deskripsi_singkat');
					      $deskripsi_lengkap   = $this->input->post('deskripsi_lengkap');
					    

					      $path = './images/gallery/';

					      $kondisi = array('id_sub' => $id_sub );

					      // get foto
					      $config['upload_path'] = './images/gallery';
					      $config['allowed_types'] = 'jpg|png|jpeg|gif';
					      $config['max_size'] = '2048';  //2MB max
					      $config['max_width'] = '4480'; // pixel
					      $config['max_height'] = '4480'; // pixel
					      $config['file_name'] = $_FILES['gambar_cof']['name'];

					      $this->upload->initialize($config);

						    if (!empty($_FILES['gambar_cof']['name'])) {
						        
						        if ( $this->upload->do_upload('gambar_cof') ) {
						            $file = $this->upload->data();
						            $data = array(
						                          'nama_sub'       => $nama_sub,
						                          'deskripsi_singkat'       => $deskripsi_singkat,
						                          'deskripsi_lengkap'       => $deskripsi_lengkap,
					                              'file'       => $file['file_name'],
						                        
						                        );
					              // hapus foto pada direktori
					              @unlink($path.$this->input->post('filelama'));

								  $this->m_home->update_sub_data($data,$kondisi);
					              redirect('admin/home/sub_us');
						        }else {
					              die("gagal update");
						        }
						    }else {
						      echo "tidak berhasil update data harap masukan foto dan data yg benar";
						    }

					  }


		 function delete_sub($id_sub,$file)
		   	
		   	{
			      $path = './images/gallery/';
			      @unlink($path.$file);

			      $where = array('id_sub' => $id_sub );
			      $this->m_home->delete_sub($where);
			      return redirect('admin/home/sub_us');
			  }

}
