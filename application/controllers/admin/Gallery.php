<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_gallery');
		 $this->load->library('upload');
		 $this->load->helper('string');
 
	}
	
	 function index()
	{
		$data['judul'] = 'Gallery Foto';
		$data['foto'] = $this->m_gallery->all();
		$this->load->view('admin/gallery-foto',$data);
	}





	public function insert_telent1()
        {
		      $nama_foto   = $this->input->post('nama_foto');
		      $deskripsi   = $this->input->post('deskripsi');
		      $status   = $this->input->post('status');
		  

		      // get foto
		      $config['upload_path'] = './images/gallery';
		      $config['allowed_types'] = 'jpg|png|jpeg|gif';
		      $config['max_size'] = '2048';  //2MB max
		      $config['max_width'] = '4480'; // pixel
		      $config['max_height'] = '4480'; // pixel
		      $config['file_name'] = $_FILES['gambar_cof']['name'];

		      $this->upload->initialize($config);

			    if (!empty($_FILES['gambar_cof']['name'])) {
			        if ( $this->upload->do_upload('gambar_cof') ) {
			            $file_gallery = $this->upload->data();
			            $data = array(
			                          'nama_foto'       => $nama_foto,
			                          'deskripsi'       => $deskripsi,
			                          'status'       => $status,
		                           	  'file_gallery'       => $file_gallery['file_name'],
			                         
			                        );
									$this->m_gallery->insert_gambar_telent($data);
		              redirect('admin/gallery');
			        }else {
		              die("gagal upload");
			        }
			    }else {
			      echo "tidak masuk";
			    }

		  }


		   public function update_telent1()
		  {
		      $id_foto   = $this->input->post('id_foto');
		      $nama_foto   = $this->input->post('nama_foto');
		      $deskripsi   = $this->input->post('deskripsi');
		      $status   = $this->input->post('status');
		    

		      $path = './images/gallery/';

		      $kondisi = array('id_foto' => $id_foto );

		      // get foto
		      $config['upload_path'] = './images/gallery';
		      $config['allowed_types'] = 'jpg|png|jpeg|gif';
		      $config['max_size'] = '2048';  //2MB max
		      $config['max_width'] = '4480'; // pixel
		      $config['max_height'] = '4480'; // pixel
		      $config['file_name'] = $_FILES['gambar_cof']['name'];

		      $this->upload->initialize($config);

			    if (!empty($_FILES['gambar_cof']['name'])) {
			        
			        if ( $this->upload->do_upload('gambar_cof') ) {
			            $file_gallery = $this->upload->data();
			            $data = array(
			                          'nama_foto'       => $nama_foto,
			                          'deskripsi'       => $deskripsi,
			                          'status'       	=> $status,
		                              'file_gallery'    => $file_gallery['file_name'],
			                        
			                        );
		              // hapus foto pada direktori
		              @unlink($path.$this->input->post('filelama'));

					  $this->m_gallery->update_telent($data,$kondisi);
		              redirect('admin/gallery');
			        }else {
		              die("gagal update");
			        }
			    }else {
			      echo "tidak masuk";
			    }

			    



		  }

		  function delete_telent1($id_foto,$file_gallery)
		   {
			      $path = './images/gallery/';
			      @unlink($path.$file_gallery);

			      $where = array('id_foto' => $id_foto );
			      $this->m_gallery->delete_telent($where);
			      return redirect('admin/gallery');
			  }



		   function vidio()
			{
				$data['judul'] = 'Gallery Vidio';
				$data['vidio'] = $this->m_gallery->all_art();
				$this->load->view('admin/gallery-vidio',$data);
			}
		 	

		 public function insert_vidio()
        {
		    	
			$configVideo['upload_path'] = 'images/gallery/'; # check path is correct
			$configVideo['max_size'] = '102400';
			$configVideo['allowed_types'] = 'mp4'; # add video extenstion on here
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$video_name = $_FILES['gambar_cof']['name'];
			$configVideo['file_name'] = $video_name;

			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);

			if (!$this->upload->do_upload('gambar_cof')) # form input field attribute
			{
			    # Upload Failed
			    $this->session->set_flashdata('error', $this->upload->display_errors());
			    redirect('admin/gallery/vidio');
			}
			else
			{
			    # Upload Successfull
			    $url = ''.$video_name;
			    $set1 =  $this->m_gallery->uploadData($url);
			    $this->session->set_flashdata('success', 'Video Has been Uploaded');
			    redirect('admin/gallery/vidio');
			}


		}


		  public function delete_vidio1($id_foto,$file_gallery)
			  {
			      $path = './images/gallery/';
			      @unlink($path.$file_gallery);

			      $where = array('id_foto' => $id_foto );
			      $this->m_gallery->delete_telent($where);
			      return redirect('admin/gallery/vidio');
			  }

}
