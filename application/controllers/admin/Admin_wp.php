<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_wp extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_login');
 
	}
	
	public function index()
	{
		$this->load->view('admin/login');
	}

	function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
	
		$where = array(
			'username' => $username,
			'password' => md5($password),
			);

		$data = $this->m_login->cek_login('tb_login',$where)->num_rows();

		if($data > 0){
 
			$data_session = array(
				'nama'  => $username,
				

				);
 
			$this->session->set_userdata($data_session);
 
			
			redirect('admin/home');
 
		}else{

			$this->session->set_flashdata('message', 'Maaf anda gagal login periksa kembali username,password dan aksesnya');
		
			redirect('admin/Admin_wp');
		}
	}
 
	function logout(){
		$this->session->sess_destroy();
		redirect('admin/Admin_wp');
	}


}
