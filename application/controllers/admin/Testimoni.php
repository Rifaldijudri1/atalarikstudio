<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_home');
		 $this->load->library('upload');
		 $this->load->helper('string');
 
	}
	
	 function index()
	{
		$data['judul'] = 'Testimoni';
		$data['testi'] = $this->m_home->testimoni();
		$this->load->view('admin/testimoni',$data);
	}

	function update_testimoni(){

	$id_testi = $this->input->post('id_testi');
	$status = $this->input->post('status');
	
		$data = array(
			'status' => $status,
			
		);
	 
		$where = array(
			'id_testi' => $id_testi
		);
	 
		$this->m_home->update_testimoni($where,$data,'tb_testimoni');
		$this->session->set_flashdata('message', 'Anda Berhasil Menganti status');
		redirect('admin/testimoni');
	}





	

}
