<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Telent extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_telent');
		 $this->load->library('upload');
 
	}
	
	 function index()
	{
		$data['judul'] = 'Telent Program Modeling';
		$data['telent'] = $this->m_telent->all();
		$this->load->view('admin/telent-modeling',$data);
	}



	public function insert_telent()
        {
		      $nama_telent   = $this->input->post('nama_telent');
		      $deskripsi_telent   = $this->input->post('deskripsi_telent');
		      $status   = $this->input->post('status');
		  

		      // get foto
		      $config['upload_path'] = './images/telent';
		      $config['allowed_types'] = 'jpg|png|jpeg|gif';
		      $config['max_size'] = '2048';  //2MB max
		      $config['max_width'] = '4480'; // pixel
		      $config['max_height'] = '4480'; // pixel
		      $config['file_name'] = $_FILES['gambar_cof']['name'];

		      $this->upload->initialize($config);

			    if (!empty($_FILES['gambar_cof']['name'])) {
			        if ( $this->upload->do_upload('gambar_cof') ) {
			            $gambar_telent = $this->upload->data();
			            $data = array(
			                          'nama_telent'       => $nama_telent,
			                          'deskripsi_telent'       => $deskripsi_telent,
			                          'status'       => $status,
		                           	  'gambar_telent'       => $gambar_telent['file_name'],
			                         
			                        );
									$this->m_telent->insert_gambar_telent($data);
		              redirect('admin/telent');
			        }else {
		              die("gagal upload");
			        }
			    }else {
			      echo "tidak masuk";
			    }

		  }


		   public function update_telent()
		  {
		      $id_telent   = $this->input->post('id_telent');
		      $nama_telent   = $this->input->post('nama_telent');
		      $deskripsi_telent   = $this->input->post('deskripsi_telent');
		      $status   = $this->input->post('status');
		    

		      $path = './images/telent/';

		      $kondisi = array('id_telent' => $id_telent );

		      // get foto
		      $config['upload_path'] = './images/telent';
		      $config['allowed_types'] = 'jpg|png|jpeg|gif';
		      $config['max_size'] = '2048';  //2MB max
		      $config['max_width'] = '4480'; // pixel
		      $config['max_height'] = '4480'; // pixel
		      $config['file_name'] = $_FILES['gambar_cof']['name'];

		      $this->upload->initialize($config);

			    if (!empty($_FILES['gambar_cof']['name'])) {
			        
			        if ( $this->upload->do_upload('gambar_cof') ) {
			            $gambar_telent = $this->upload->data();
			            $data = array(
			                          'nama_telent'       => $nama_telent,
			                          'deskripsi_telent'       => $deskripsi_telent,
			                          'status'       => $status,
		                              'gambar_telent'       => $gambar_telent['file_name'],
			                        
			                        );
		              // hapus foto pada direktori
		              @unlink($path.$this->input->post('filelama'));

					  $this->m_telent->update_telent($data,$kondisi);
		              redirect('admin/telent');
			        }else {
		              die("gagal update");
			        }
			    }else {
			      echo "tidak masuk";
			    }

			    



		  }

		  function delete_telent($id_telent)
		  {
		      $path = './images/telent/'.$gambar_telent;
              unlink($path);

		      $where = array('id_telent' => $id_telent );
		      $this->m_telent->delete_telent($where);
		      return redirect('admin/telent');
		  }




	 function acting()
		{
			$data['judul'] = 'Telent Program Acting';
			$data['telent'] = $this->m_telent->all_art();
			$this->load->view('admin/telent-acting',$data);
		}



	public function insert_telent1()
        {
		      $nama_telent   = $this->input->post('nama_telent');
		      $deskripsi_telent   = $this->input->post('deskripsi_telent');
		      $status   = $this->input->post('status');
		  

		      // get foto
		      $config['upload_path'] = './images/telent';
		      $config['allowed_types'] = 'jpg|png|jpeg|gif';
		      $config['max_size'] = '2048';  //2MB max
		      $config['max_width'] = '4480'; // pixel
		      $config['max_height'] = '4480'; // pixel
		      $config['file_name'] = $_FILES['gambar_cof']['name'];

		      $this->upload->initialize($config);

			    if (!empty($_FILES['gambar_cof']['name'])) {
			        if ( $this->upload->do_upload('gambar_cof') ) {
			            $gambar_telent = $this->upload->data();
			            $data = array(
			                          'nama_telent'       => $nama_telent,
			                          'deskripsi_telent'       => $deskripsi_telent,
			                          'status'       => $status,
		                           	  'gambar_telent'       => $gambar_telent['file_name'],
			                         
			                        );
									$this->m_telent->insert_gambar_telent($data);
		              redirect('admin/telent/acting');
			        }else {
		              die("gagal upload");
			        }
			    }else {
			      echo "tidak masuk";
			    }

		  }


		   public function update_telent1()
		  {
		      $id_telent   = $this->input->post('id_telent');
		      $nama_telent   = $this->input->post('nama_telent');
		      $deskripsi_telent   = $this->input->post('deskripsi_telent');
		      $status   = $this->input->post('status');
		    

		      $path = './images/telent/';

		      $kondisi = array('id_telent' => $id_telent );

		      // get foto
		      $config['upload_path'] = './images/telent';
		      $config['allowed_types'] = 'jpg|png|jpeg|gif';
		      $config['max_size'] = '2048';  //2MB max
		      $config['max_width'] = '4480'; // pixel
		      $config['max_height'] = '4480'; // pixel
		      $config['file_name'] = $_FILES['gambar_cof']['name'];

		      $this->upload->initialize($config);

			    if (!empty($_FILES['gambar_cof']['name'])) {
			        
			        if ( $this->upload->do_upload('gambar_cof') ) {
			            $gambar_telent = $this->upload->data();
			            $data = array(
			                          'nama_telent'       => $nama_telent,
			                          'deskripsi_telent'       => $deskripsi_telent,
			                          'status'       => $status,
		                              'gambar_telent'       => $gambar_telent['file_name'],
			                        
			                        );
		              // hapus foto pada direktori
		              @unlink($path.$this->input->post('filelama'));

					  $this->m_telent->update_telent($data,$kondisi);
		              redirect('admin/telent/acting');
			        }else {
		              die("gagal update");
			        }
			    }else {
			      echo "tidak masuk";
			    }

			    



		  }

		  function delete_telent1($id_telent)
		  {
		      $path = './images/telent/'.$gambar_telent;
              unlink($path);

		      $where = array('id_telent' => $id_telent );
		      $this->m_telent->delete_telent($where);
		      return redirect('admin/telent/acting');
		  }


		 



}
