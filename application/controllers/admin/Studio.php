<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Studio extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_studio');
		 $this->load->library('upload');
 
	}
	
	 function index()
	{
		$data['judul'] = 'Studio Creative';
		$data['studio'] = $this->m_studio->all();
		$this->load->view('admin/studio-creative',$data);
	}



	public function insert_studio()
        {
		      $nama_studio   = $this->input->post('nama_studio');
		      $deskripsi_studio   = $this->input->post('deskripsi_studio');
		      $status   = $this->input->post('status');
		  

		      // get foto
		      $config['upload_path'] = './images/studio';
		      $config['allowed_types'] = 'jpg|png|jpeg|gif';
		      $config['max_size'] = '2048';  //2MB max
		      $config['max_width'] = '4480'; // pixel
		      $config['max_height'] = '4480'; // pixel
		      $config['file_name'] = $_FILES['gambar_cof']['name'];

		      $this->upload->initialize($config);

			    if (!empty($_FILES['gambar_cof']['name'])) {
			        if ( $this->upload->do_upload('gambar_cof') ) {
			            $gambar_studio = $this->upload->data();
			            $data = array(
			                          'nama_studio'       => $nama_studio,
			                          'deskripsi_studio'       => $deskripsi_studio,
			                          'status'       => $status,
		                           	  'gambar_studio'       => $gambar_studio['file_name'],
			                         
			                        );
									$this->m_studio->insert_gambar_studio($data);
		              redirect('admin/studio');
			        }else {
		              die("gagal upload");
			        }
			    }else {
			      echo "tidak masuk";
			    }

		  }


		   public function update_studio_gambar()
		  {
		    

				$id_studio   = $this->input->post('id_studio');
				$nama_studio   = $this->input->post('nama_studio');
				$deskripsi_studio   = $this->input->post('deskripsi_studio');
				$status   = $this->input->post('status');
		    

			      $path = './images/studio/';

			      $kondisi = array('id_studio' => $id_studio );

			      // get foto
			      $config['upload_path'] = './images/studio';
			      $config['allowed_types'] = 'jpg|png|jpeg|gif';
			      $config['max_size'] = '2048';  //2MB max
			      $config['max_width'] = '4480'; // pixel
			      $config['max_height'] = '4480'; // pixel
			      $config['file_name'] = $_FILES['gambar_cof']['name'];

			      $this->upload->initialize($config);

				    if (!empty($_FILES['gambar_cof']['name'])) {
				        if ( $this->upload->do_upload('gambar_cof') ) {
				            $gambar_studio = $this->upload->data();
				            $data = array(
									'nama_studio'       => $nama_studio,
									'deskripsi_studio'  => $deskripsi_studio,
									'status'       	=> $status,
									'gambar_studio'       => $gambar_studio['file_name'],
				                        
				                        );
			              // hapus foto pada direktori
			              @unlink($path.$this->input->post('filelama'));

							$this->m_studio->update_studio_gambar($data,$kondisi);
			              redirect('admin/studio');
				        }else {
			              die("gagal update");
				        }
				    }else {
				      echo "tidak masuk";
				    }


		  }

		  function delete_studio($id_studio)
		  {
		      $path = './images/'.$gambar_studio;
              unlink($path);

		      $where = array('id_studio' => $id_studio );
		      $this->m_studio->delete_studio($where);
		      return redirect('admin/studio');
		  }




		  function index_art()
			{
				$data['judul'] = 'Studio Art';
				$data['studio'] = $this->m_studio->all_art();
				$this->load->view('admin/studio-art',$data);
			}


			public function insert_studio_art()
        {
		      $nama_studio   = $this->input->post('nama_studio');
		      $deskripsi_studio   = $this->input->post('deskripsi_studio');
		      $status   = $this->input->post('status');
		  

		      // get foto
		      $config['upload_path'] = './images/studio';
		      $config['allowed_types'] = 'jpg|png|jpeg|gif';
		      $config['max_size'] = '2048';  //2MB max
		      $config['max_width'] = '4480'; // pixel
		      $config['max_height'] = '4480'; // pixel
		      $config['file_name'] = $_FILES['gambar_cof']['name'];

		      $this->upload->initialize($config);

			    if (!empty($_FILES['gambar_cof']['name'])) {
			        if ( $this->upload->do_upload('gambar_cof') ) {
			            $gambar_studio = $this->upload->data();
			            $data = array(
			                          'nama_studio'       => $nama_studio,
			                          'deskripsi_studio'       => $deskripsi_studio,
			                          'status'       => $status,
		                           	  'gambar_studio'       => $gambar_studio['file_name'],
			                         
			                        );
									$this->m_studio->insert_gambar_studio($data);
		              redirect('admin/studio/index_art');
			        }else {
		              die("gagal upload");
			        }
			    }else {
			      echo "tidak masuk";
			    }

		  }


		   public function update_studio_gambar_art()
		  {
		      $id_studio   = $this->input->post('id_studio');
		      $nama_studio   = $this->input->post('nama_studio');
		      $deskripsi_studio   = $this->input->post('deskripsi_studio');
		      $status   = $this->input->post('status');
		    

		      $path = './images/studio/';

		      $kondisi = array('id_studio' => $id_studio );

		      // get foto
		      $config['upload_path'] = './images/studio';
		      $config['allowed_types'] = 'jpg|png|jpeg|gif';
		      $config['max_size'] = '2048';  //2MB max
		      $config['max_width'] = '4480'; // pixel
		      $config['max_height'] = '4480'; // pixel
		      $config['file_name'] = $_FILES['gambar_cof']['name'];

		      $this->upload->initialize($config);

			    if (!empty($_FILES['gambar_cof']['name'])) {
			        if ( $this->upload->do_upload('gambar_cof') ) {
			            $gambar_studio = $this->upload->data();
			            $data = array(
			                          'nama_studio'       => $nama_studio,
			                          'deskripsi_studio'       => $deskripsi_studio,
			                          'status'       => $status,
		                              'gambar_studio'       => $gambar_studio['file_name'],
			                        
			                        );
		              // hapus foto pada direktori
		              @unlink($path.$this->input->post('filelama'));

					  $this->m_studio->update_studio_gambar($data,$kondisi);
		              redirect('admin/studio/index_art');
			        }else {
		              die("gagal update");
			        }
			    }else {
			      echo "tidak masuk";
			    }

		  }

		  function delete_studio_art($id_studio)
		  {
		      $path = './images/'.$gambar_studio;
              unlink($path);

		      $where = array('id_studio' => $id_studio );
		      $this->m_studio->delete_studio($where);
		      return redirect('admin/studio/index_art');
		  }



}
