<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Caffe extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_home');
 
	}
	
	public function index()
	{	
	    $data['setting'] = $this->m_home->about_us();
		$data['caffe'] = $this->m_home->caffe();
		$data['caffe_img'] = $this->m_home->caffe_gambar();
		$this->load->view('caffe-work',$data);
	}


}
