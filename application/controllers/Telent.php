<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Telent extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_home');
		$this->load->model('m_telent');
 
	}
	
	public function index()
	{	
	    $data['setting'] = $this->m_home->about_us();
		//$data['caffe'] = $this->m_home->caffe();
		$data['telent'] = $this->m_telent->all();
		$this->load->view('telent-modeling',$data);
	}


	public function telent_acting()
	{	
	    $data['setting'] = $this->m_home->about_us();
		//$data['caffe'] = $this->m_home->caffe();
		$data['telent'] = $this->m_telent->all_art();
		$this->load->view('telent-acting',$data);
	}


}
