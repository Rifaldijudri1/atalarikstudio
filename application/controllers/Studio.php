<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Studio extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_home');
		$this->load->model('m_studio');
 
	}
	
	public function index()
	{	
	    $data['setting'] = $this->m_home->about_us();
		//$data['caffe'] = $this->m_home->caffe();
		$data['studio'] = $this->m_studio->all();
		$this->load->view('creative-studio',$data);
	}


	public function art_studio()
	{	
	    $data['setting'] = $this->m_home->about_us();
		//$data['caffe'] = $this->m_home->caffe();
		$data['studio'] = $this->m_studio->all_art();
		$this->load->view('art-studio',$data);
	}


}
