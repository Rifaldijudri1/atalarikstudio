<?php 
 
class M_daftar extends CI_Model{	


	function all()
	{
		
		$this->db->from("tb_pendaftaraan");
        $this->db->order_by("id_pendaftaran", "desc");
        $query = $this->db->get();
        return $query->result();
	}



	  function insert_gambar_telent($data)
	  {
	      $this->db->insert('tb_pendaftaraan',$data);
	      return TRUE;
	  }

	function update_telent($data,$kondisi)
	  {
	      $this->db->update('tb_pendaftaraan',$data,$kondisi);
	      return TRUE;
	  }


	  function delete_telent($where)
	  {
	      $this->db->where($where);
	      $this->db->delete('tb_pendaftaraan');
	      return TRUE;
	  }

	  public function uploadData($url)
		{
		    $nama_foto = $this->input->post('nama_foto');
		    $deskripsi = $this->input->post('deskripsi');
		    $status = $this->input->post('status');

		    $data = array(
		        'file_gallery'       => $url,
		        'nama_foto'     => $nama_foto,
		        'deskripsi'   => $deskripsi,
		        'status'  => $status
		    );

		    $this->db->insert('tb_pendaftaraan', $data);
		}


}