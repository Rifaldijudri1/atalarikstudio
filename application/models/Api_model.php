<?php
class Api_model extends CI_Model
{
	function barang()
	{
		$this->db->order_by('id_barang', 'DESC');
		return $this->db->get('tb_barang');
	}

	function insert_api($data)
	{
		$this->db->insert('tb_barang', $data);
	}

	function fetch_single_user($id_barang)
	{
		$this->db->where('id_barang', $id_barang);
		$query = $this->db->get('tb_barang');
		return $query->result_array();
	}

	function update_api($id_barang, $data)
	{
		$this->db->where('id_barang', $id_barang);
		$this->db->update('tb_barang', $data);
	}

	function delete_single_user($id_barang)
	{
		$this->db->where('id_barang', $id_barang);
		$this->db->delete('tb_barang');
		if($this->db->affected_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	function list_pengguna()
	{
		$this->db->order_by('id_login', 'DESC');
		return $this->db->get('tb_login');
	}

	function insert_api_pegguna($data)
	{
		$this->db->insert('tb_login', $data);
	}

	function fetch_single_user_pengguna($id_login)
	{
		$this->db->where('id_login', $id_login);
		$query = $this->db->get('tb_login');
		return $query->result_array();
	}

	function update_api_pengguna($id_login, $data)
	{
		$this->db->where('id_login', $id_login);
		$this->db->update('tb_login', $data);
	}

	function delete_single_user_pengguna($id_login)
	{
			
		$this->db->where('id_login', $id_login);
		$this->db->delete('tb_login');
		if($this->db->affected_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

?>