<?php 
 
class M_telent extends CI_Model{	


	function all()
	{
		$this->db->from("tb_telent");
        $this->db->where("status", "modeling");
        $query = $this->db->get();
        return $query->result();
	}

	function all_art()
	{
		$this->db->from("tb_telent");
        $this->db->where("status", "acting");
        $query = $this->db->get();
        return $query->result();
	}


	  function insert_gambar_telent($data)
	  {
	      $this->db->insert('tb_telent',$data);
	      return TRUE;
	  }

	function update_telent($data,$kondisi)
	  {
	      $this->db->update('tb_telent',$data,$kondisi);
	      return TRUE;
	  }


	  function delete_telent($where)
	  {
	      $this->db->where($where);
	      $this->db->delete('tb_telent');
	      return TRUE;
	  }


}