<?php 
 
class M_Home extends CI_Model{	


	function slide()
	{
		$this->db->from("tb_slide");
        $this->db->order_by("id_slide", "desc");
        $query = $this->db->get();
        return $query->result();
	}


	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}	

	function syarat()
	{
		$this->db->from("tb_syarat");
        $this->db->order_by("id_syarat", "desc");
        $query = $this->db->get();
        return $query->result();
	}

	function update_data_syarat($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}	

	function about_us()
	{
		$this->db->from("tb_websetting");
        $this->db->order_by("id_websetting", "desc");
        $query = $this->db->get();
        return $query->result();
	}

	function update_data_about_us($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}	

	function contact_us()
	{
		$this->db->from("tb_websetting");
        $this->db->order_by("id_websetting", "desc");
        $query = $this->db->get();
        return $query->result();
	}

	function update_data_contact_us($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}	


	function caffe()
	{
		$this->db->from("tb_coffe");
        $this->db->order_by("id_coffe", "desc");
        $query = $this->db->get();
        return $query->result();
	}

	function update_data_caffe($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}	


	function caffe_gambar()
	{
		$this->db->from("tb_gallery_coffe");
        $this->db->order_by("id_gambar_coffe", "desc");
        $query = $this->db->get();
        return $query->result();
	}

	  function insert_gambar_caffe($data)
	  {
	      $this->db->insert('tb_gallery_coffe',$data);
	      return TRUE;
	  }

    function update_coffe_gambar($data,$kondisi)
	  {
	      $this->db->update('tb_gallery_coffe',$data,$kondisi);
	      return TRUE;
	  }

	  function delete_coffe($where)
	  {	
	  	  
	      $this->db->where($where);
	      $this->db->delete('tb_gallery_coffe');

	      
	      
	      return TRUE;
	  }


	function testimoni_show()
	{
		$this->db->from("tb_testimoni");
        $this->db->where("status", "aktif");
        $query = $this->db->get();
        return $query->result();
	}

	  function testimoni()
	{
		$this->db->from("tb_testimoni");
        $this->db->order_by("id_testi", "desc");
        $query = $this->db->get();
        return $query->result();
	}


	  function testimoni_all($data)
	  {
	      $this->db->insert('tb_testimoni',$data);
	      return TRUE;
	  }


	  function update_testimoni($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}	


	function all_sub()
	{
		$this->db->from("tb_sub");
        $this->db->order_by("id_sub", "desc");
        $query = $this->db->get();
        return $query->result();
	}

	 function insert_gambar_sub($data)
	  {
	      $this->db->insert('tb_sub',$data);
	      return TRUE;
	  }

	  function update_sub_data($data,$kondisi)
	  {
	      $this->db->update('tb_sub',$data,$kondisi);
	      return TRUE;
	  }


	  function delete_sub($where)
	  {
	      $this->db->where($where);
	      $this->db->delete('tb_sub');
	      return TRUE;
	  }


}