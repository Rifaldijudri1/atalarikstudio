<?php 
 
class M_gallery extends CI_Model{	


	function all()
	{
		$this->db->from("tb_gallery_all");
        $this->db->where("status", "foto");
        $query = $this->db->get();
        return $query->result();
	}

	function all_art()
	{
		$this->db->from("tb_gallery_all");
        $this->db->where("status", "vidio");
        $query = $this->db->get();
        return $query->result();
	}


	  function insert_gambar_telent($data)
	  {
	      $this->db->insert('tb_gallery_all',$data);
	      return TRUE;
	  }

	function update_telent($data,$kondisi)
	  {
	      $this->db->update('tb_gallery_all',$data,$kondisi);
	      return TRUE;
	  }


	  function delete_telent($where)
	  {
	      $this->db->where($where);
	      $this->db->delete('tb_gallery_all');
	      return TRUE;
	  }

	  public function uploadData($url)
		{
		    $nama_foto = $this->input->post('nama_foto');
		    $deskripsi = $this->input->post('deskripsi');
		    $status = $this->input->post('status');

		    $data = array(
		        'file_gallery'       => $url,
		        'nama_foto'     => $nama_foto,
		        'deskripsi'   => $deskripsi,
		        'status'  => $status
		    );

		    $this->db->insert('tb_gallery_all', $data);
		}


}