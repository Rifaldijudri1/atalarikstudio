<?php 
 
class M_studio extends CI_Model{	


	function all()
	{
		$this->db->from("tb_studio");
        $this->db->where("status", "creative_studio");
        $query = $this->db->get();
        return $query->result();
	}

	function all_art()
	{
		$this->db->from("tb_studio");
        $this->db->where("status", "art_studio");
        $query = $this->db->get();
        return $query->result();
	}


	  function insert_gambar_studio($data)
	  {
	      $this->db->insert('tb_studio',$data);
	      return TRUE;
	  }

    function update_studio_gambar($data,$kondisi)
	  {
	      $this->db->update('tb_studio',$data,$kondisi);
	      return TRUE;
	  }

	  function delete_studio($where)
	  {
	      $this->db->where($where);
	      $this->db->delete('tb_studio');
	      return TRUE;
	  }


}