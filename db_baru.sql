/*
 Navicat Premium Data Transfer

 Source Server         : localhost_db
 Source Server Type    : MySQL
 Source Server Version : 50625
 Source Host           : localhost:3306
 Source Schema         : db_atalarikworkshop

 Target Server Type    : MySQL
 Target Server Version : 50625
 File Encoding         : 65001

 Date: 24/12/2021 01:22:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_coffe
-- ----------------------------
DROP TABLE IF EXISTS `tb_coffe`;
CREATE TABLE `tb_coffe`  (
  `id_coffe` int(11) NOT NULL AUTO_INCREMENT,
  `nama_coffe` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi_coffe` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_coffe` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contact_coffe` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_coffe` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_gambar` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_coffe`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_coffe
-- ----------------------------
INSERT INTO `tb_coffe` VALUES (1, 'CAFFE WORK SKY LOUNGE', 'Caffe Kekinian', 'Cibinong', '0829992891', 'email@gmail.com', NULL);

-- ----------------------------
-- Table structure for tb_gallery_all
-- ----------------------------
DROP TABLE IF EXISTS `tb_gallery_all`;
CREATE TABLE `tb_gallery_all`  (
  `id_foto` int(11) NOT NULL AUTO_INCREMENT,
  `nama_foto` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_gallery` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('foto','vidio') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_foto`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tb_gallery_coffe
-- ----------------------------
DROP TABLE IF EXISTS `tb_gallery_coffe`;
CREATE TABLE `tb_gallery_coffe`  (
  `id_gambar_coffe` int(11) NOT NULL AUTO_INCREMENT,
  `gambar_coffe` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_gambar` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_gambar_coffe`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tb_login
-- ----------------------------
DROP TABLE IF EXISTS `tb_login`;
CREATE TABLE `tb_login`  (
  `id_login` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `level` enum('admin','super') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_login`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_login
-- ----------------------------
INSERT INTO `tb_login` VALUES (1, 'rifaldi', 'e10adc3949ba59abbe56e057f20f883e', 'Rifaldi Judri', 'super');
INSERT INTO `tb_login` VALUES (2, 'admin', '7488e331b8b64e5794da3fa4eb10ad5d', 'Admin', 'super');
INSERT INTO `tb_login` VALUES (4, 'super', '1b3231655cebb7a1f783eddf27d254ca', 'Super', 'super');

-- ----------------------------
-- Table structure for tb_pendaftaraan
-- ----------------------------
DROP TABLE IF EXISTS `tb_pendaftaraan`;
CREATE TABLE `tb_pendaftaraan`  (
  `id_pendaftaran` int(11) NOT NULL AUTO_INCREMENT,
  `nama_cust` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_cust` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_telp` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `umur` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nik` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `foto_cust` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `foto_ktp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_cust` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_pendaftaran`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tb_slide
-- ----------------------------
DROP TABLE IF EXISTS `tb_slide`;
CREATE TABLE `tb_slide`  (
  `id_slide` int(11) NOT NULL AUTO_INCREMENT,
  `subtitle` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_content` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi_slide` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_slide`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_slide
-- ----------------------------
INSERT INTO `tb_slide` VALUES (1, 'PRE-LAUNCH EVENT', 'ATALARIK WORKSHOP', 'Yuk Belajar Akting Bareng Atalarik Syach!!');

-- ----------------------------
-- Table structure for tb_studio
-- ----------------------------
DROP TABLE IF EXISTS `tb_studio`;
CREATE TABLE `tb_studio`  (
  `id_studio` int(11) NOT NULL AUTO_INCREMENT,
  `nama_studio` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi_studio` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `gambar_studio` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('creative_studio','art_studio') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_studio`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tb_syarat
-- ----------------------------
DROP TABLE IF EXISTS `tb_syarat`;
CREATE TABLE `tb_syarat`  (
  `id_syarat` int(11) NOT NULL AUTO_INCREMENT,
  `nama_judul` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `syarat` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_syarat`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_syarat
-- ----------------------------
INSERT INTO `tb_syarat` VALUES (1, 'Yuk Belajar Akting Bersama Atalarik Syach!', 'Boleh siapa aja yang ikut asal warga negara indonesia ya guys');

-- ----------------------------
-- Table structure for tb_telent
-- ----------------------------
DROP TABLE IF EXISTS `tb_telent`;
CREATE TABLE `tb_telent`  (
  `id_telent` int(11) NOT NULL AUTO_INCREMENT,
  `nama_telent` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi_telent` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `gambar_telent` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('modeling','acting') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_telent`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tb_testimoni
-- ----------------------------
DROP TABLE IF EXISTS `tb_testimoni`;
CREATE TABLE `tb_testimoni`  (
  `id_testi` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` enum('aktif','nonaktif') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_testi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_testimoni
-- ----------------------------
INSERT INTO `tb_testimoni` VALUES (3, 'MUHAMMAD RIFALDI JUDRI', 'muhamadrifaldi145@gmail.com', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 'aktif', '2021-12-23');
INSERT INTO `tb_testimoni` VALUES (4, 'Baim', 'baim@gmail.com', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 'aktif', '2021-12-23');

-- ----------------------------
-- Table structure for tb_websetting
-- ----------------------------
DROP TABLE IF EXISTS `tb_websetting`;
CREATE TABLE `tb_websetting`  (
  `id_websetting` int(11) NOT NULL AUTO_INCREMENT,
  `contact_us` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `about_us` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `address` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `faq` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_websetting`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_websetting
-- ----------------------------
INSERT INTO `tb_websetting` VALUES (1, '081272053905', 'support@gmail.com', 'Alima Production dibentuk tahun 2020, dibawah naungan yayasan Alima Fajar Sentosa berlokasi tetap di kawasan Cibinong City Center, memandang perlu membuat sebuah rumah produksi dimasa bertambah luasnya media entertainment di era digital saat ini dengan adanya medsos dan layanan OTT seperti web tv, web series, dll.\r\nAlima masih sangat baru namun memiliki kepercayaan yang besar dapat bersaing dibidangnya dikarenakan Alima Production mengusung nama nama yang sudah tidak asing di dunia entertainment tanah air seperti Teddy Syach, Atalarik Syach, Attila Syach dan Attar Syach yang biasa dikenal orang Syach Brothers.\r\nAlima Production dengan tag line atau premisenya yaitu “ We Creat/ based on/ in The Best Quality Throuh The Brightest Creativity” melihat fenomena dari perkembangan dunia entertainment tanah air yang membuka peluang dalam menciptakan karya karya yang bisa langsung dilihat dan dinikmati dalam hitungan detik, namun Alima Production melihat kemudahan itu tetap diperlukan atensi sehingga kami selalu terpacu untuk berkarya dan berkembang dalam kualitas, yang nantinya karya kami bukan hanya sekedar karya sekedar diingat tapi karya yang selalu dapat dinikmati, karya abadi dari budaya anak negeri.', 'Komplek Cibinong City Center Jl. Raya Tegar Beriman no. 1 blok C21, 16915', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su');

SET FOREIGN_KEY_CHECKS = 1;
