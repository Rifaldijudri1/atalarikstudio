/*
 Navicat Premium Data Transfer

 Source Server         : localhost_db
 Source Server Type    : MySQL
 Source Server Version : 50625
 Source Host           : localhost:3306
 Source Schema         : db_atalarikworkshop

 Target Server Type    : MySQL
 Target Server Version : 50625
 File Encoding         : 65001

 Date: 16/12/2021 17:59:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_coffe
-- ----------------------------
DROP TABLE IF EXISTS `tb_coffe`;
CREATE TABLE `tb_coffe`  (
  `id_coffe` int(11) NOT NULL AUTO_INCREMENT,
  `nama_coffe` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi_coffe` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_coffe` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contact_coffe` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_coffe` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_coffe`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tb_gallery_foto
-- ----------------------------
DROP TABLE IF EXISTS `tb_gallery_foto`;
CREATE TABLE `tb_gallery_foto`  (
  `id_foto` int(11) NOT NULL,
  `nama_foto` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_foto` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_foto`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tb_gallery_vidio
-- ----------------------------
DROP TABLE IF EXISTS `tb_gallery_vidio`;
CREATE TABLE `tb_gallery_vidio`  (
  `id_vidio` int(11) NOT NULL AUTO_INCREMENT,
  `nama_vidio` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_vidio` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_vidio`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tb_login
-- ----------------------------
DROP TABLE IF EXISTS `tb_login`;
CREATE TABLE `tb_login`  (
  `id_login` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `level` enum('admin','super') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_login`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_login
-- ----------------------------
INSERT INTO `tb_login` VALUES (1, 'rifaldi', 'e10adc3949ba59abbe56e057f20f883e', 'Rifaldi Judri', 'super');

-- ----------------------------
-- Table structure for tb_slide
-- ----------------------------
DROP TABLE IF EXISTS `tb_slide`;
CREATE TABLE `tb_slide`  (
  `id_slide` int(11) NOT NULL AUTO_INCREMENT,
  `subtitle` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_content` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi_slide` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_slide`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_slide
-- ----------------------------
INSERT INTO `tb_slide` VALUES (1, 'PRE-LAUNCH EVENT', 'ATALARIK WORKSHOP', 'Yuk Belajar Akting Bareng Atalarik Syach!');

-- ----------------------------
-- Table structure for tb_syarat
-- ----------------------------
DROP TABLE IF EXISTS `tb_syarat`;
CREATE TABLE `tb_syarat`  (
  `id_syarat` int(11) NOT NULL AUTO_INCREMENT,
  `nama_judul` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `syarat` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_syarat`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_syarat
-- ----------------------------
INSERT INTO `tb_syarat` VALUES (1, 'Yuk Belajar Akting Bersama Atalarik Syach1', 'Boleh siapa aja yang ikut asal warga negara indonesia');

-- ----------------------------
-- Table structure for tb_websetting
-- ----------------------------
DROP TABLE IF EXISTS `tb_websetting`;
CREATE TABLE `tb_websetting`  (
  `id_websetting` int(11) NOT NULL AUTO_INCREMENT,
  `contact_us` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `about_us` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `address` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `faq` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_websetting`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_websetting
-- ----------------------------
INSERT INTO `tb_websetting` VALUES (1, '081272053905', 'support@gmail.com', 'Alima Production dibentuk tahun 2020, dibawah naungan yayasan Alima Fajar Sentosa berlokasi tetap di kawasan Cibinong City Center, memandang perlu membuat sebuah rumah produksi dimasa bertambah luasnya media entertainment di era digital saat ini dengan adanya medsos dan layanan OTT seperti web tv, web series, dll.\r\n\r\nAlima masih sangat baru namun memiliki kepercayaan yang besar dapat bersaing dibidangnya dikarenakan Alima Production mengusung nama nama yang sudah tidak asing di dunia entertainment tanah air seperti Teddy Syach, Atalarik Syach, Attila Syach dan Attar Syach yang biasa dikenal orang Syach Brothers. Alima Production dengan tag line atau premisenya yaitu “ We Creat/ based on/ in The Best Quality Throuh The Brightest Creativity” melihat fenomena dari perkembangan dunia entertainment tanah air yang membuka peluang dalam menciptakan karya karya yang bisa langsung dilihat dan dinikmati dalam hitungan detik, namun Alima Production melihat kemudahan itu tetap diperlukan atensi sehingga kami selalu terpacu untuk berkarya dan berkembang dalam kualitas, yang nantinya karya kami bukan hanya sekedar karya sekedar diingat tapi karya yang selalu dapat dinikmati, karya abadi dari budaya anak negeri.', 'Komplek Cibinong City Center Jl. Raya Tegar Beriman no. 1 blok C21, 16915', '--');

SET FOREIGN_KEY_CHECKS = 1;
